﻿using System.Windows.Forms;
/*
 * Created by SharpDevelop.
 * User: lumar
 * Date: 03/03/2020
 * Time: 08:54 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
namespace ia_mapa
{
	partial class MainForm
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.mapaPbox = new System.Windows.Forms.PictureBox();
			this.opcionesTabControl = new System.Windows.Forms.TabControl();
			this.texturasTab = new System.Windows.Forms.TabPage();
			this.guardarTexturasBtn = new System.Windows.Forms.Button();
			this.texturasPanel = new System.Windows.Forms.Panel();
			this.pesonajesTab = new System.Windows.Forms.TabPage();
			this.distanciaButton = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tipoBusquedaCB = new System.Windows.Forms.ComboBox();
			this.ordenMovimientoBtn = new System.Windows.Forms.Button();
			this.guardarPersonajesBtn = new System.Windows.Forms.Button();
			this.deletePersonajeBtn = new System.Windows.Forms.Button();
			this.personajesPanel = new System.Windows.Forms.Panel();
			this.addPersonajeBtn = new System.Windows.Forms.Button();
			this.showTreeView = new System.Windows.Forms.TabPage();
			this.treeBusqueda = new System.Windows.Forms.TreeView();
			this.abrirMapa = new System.Windows.Forms.Button();
			this.columnasPBox = new System.Windows.Forms.PictureBox();
			this.filasPBox = new System.Windows.Forms.PictureBox();
			this.infoBtn = new System.Windows.Forms.Button();
			this.playBtn = new System.Windows.Forms.Button();
			this.controlArchivoGB = new System.Windows.Forms.GroupBox();
			this.limpiartBtn = new System.Windows.Forms.Button();
			this.filasPBox2 = new System.Windows.Forms.PictureBox();
			this.columnasPBox2 = new System.Windows.Forms.PictureBox();
			this.buscarCasillaGP = new System.Windows.Forms.GroupBox();
			this.busquedaBTN = new System.Windows.Forms.Button();
			this.busquedaTxtBox = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.mapaPbox)).BeginInit();
			this.opcionesTabControl.SuspendLayout();
			this.texturasTab.SuspendLayout();
			this.pesonajesTab.SuspendLayout();
			this.showTreeView.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.columnasPBox)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.filasPBox)).BeginInit();
			this.controlArchivoGB.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.filasPBox2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.columnasPBox2)).BeginInit();
			this.buscarCasillaGP.SuspendLayout();
			this.SuspendLayout();
			// 
			// mapaPbox
			// 
			this.mapaPbox.Location = new System.Drawing.Point(42, 41);
			this.mapaPbox.Name = "mapaPbox";
			this.mapaPbox.Size = new System.Drawing.Size(740, 670);
			this.mapaPbox.TabIndex = 0;
			this.mapaPbox.TabStop = false;
			// 
			// opcionesTabControl
			// 
			this.opcionesTabControl.Controls.Add(this.texturasTab);
			this.opcionesTabControl.Controls.Add(this.pesonajesTab);
			this.opcionesTabControl.Controls.Add(this.showTreeView);
			this.opcionesTabControl.Location = new System.Drawing.Point(822, 88);
			this.opcionesTabControl.Name = "opcionesTabControl";
			this.opcionesTabControl.SelectedIndex = 0;
			this.opcionesTabControl.Size = new System.Drawing.Size(305, 652);
			this.opcionesTabControl.TabIndex = 1;
			// 
			// texturasTab
			// 
			this.texturasTab.Controls.Add(this.guardarTexturasBtn);
			this.texturasTab.Controls.Add(this.texturasPanel);
			this.texturasTab.Location = new System.Drawing.Point(4, 22);
			this.texturasTab.Name = "texturasTab";
			this.texturasTab.Padding = new System.Windows.Forms.Padding(3);
			this.texturasTab.Size = new System.Drawing.Size(297, 626);
			this.texturasTab.TabIndex = 0;
			this.texturasTab.Text = "Texturas";
			this.texturasTab.UseVisualStyleBackColor = true;
			// 
			// guardarTexturasBtn
			// 
			this.guardarTexturasBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("guardarTexturasBtn.BackgroundImage")));
			this.guardarTexturasBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.guardarTexturasBtn.Location = new System.Drawing.Point(6, 6);
			this.guardarTexturasBtn.Name = "guardarTexturasBtn";
			this.guardarTexturasBtn.Size = new System.Drawing.Size(36, 36);
			this.guardarTexturasBtn.TabIndex = 2;
			this.guardarTexturasBtn.UseVisualStyleBackColor = true;
			this.guardarTexturasBtn.Click += new System.EventHandler(this.GuardarBtnClick);
			// 
			// texturasPanel
			// 
			this.texturasPanel.AutoScroll = true;
			this.texturasPanel.Location = new System.Drawing.Point(0, 48);
			this.texturasPanel.Name = "texturasPanel";
			this.texturasPanel.Size = new System.Drawing.Size(297, 575);
			this.texturasPanel.TabIndex = 1;
			// 
			// pesonajesTab
			// 
			this.pesonajesTab.Controls.Add(this.distanciaButton);
			this.pesonajesTab.Controls.Add(this.label1);
			this.pesonajesTab.Controls.Add(this.tipoBusquedaCB);
			this.pesonajesTab.Controls.Add(this.ordenMovimientoBtn);
			this.pesonajesTab.Controls.Add(this.guardarPersonajesBtn);
			this.pesonajesTab.Controls.Add(this.deletePersonajeBtn);
			this.pesonajesTab.Controls.Add(this.personajesPanel);
			this.pesonajesTab.Controls.Add(this.addPersonajeBtn);
			this.pesonajesTab.Location = new System.Drawing.Point(4, 22);
			this.pesonajesTab.Name = "pesonajesTab";
			this.pesonajesTab.Padding = new System.Windows.Forms.Padding(3);
			this.pesonajesTab.Size = new System.Drawing.Size(297, 626);
			this.pesonajesTab.TabIndex = 1;
			this.pesonajesTab.Text = "Personajes";
			this.pesonajesTab.UseVisualStyleBackColor = true;
			// 
			// distanciaButton
			// 
			this.distanciaButton.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("distanciaButton.BackgroundImage")));
			this.distanciaButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.distanciaButton.Location = new System.Drawing.Point(273, 22);
			this.distanciaButton.Name = "distanciaButton";
			this.distanciaButton.Size = new System.Drawing.Size(21, 23);
			this.distanciaButton.TabIndex = 7;
			this.distanciaButton.UseVisualStyleBackColor = true;
			this.distanciaButton.Click += new System.EventHandler(this.DistanciaButtonClick);
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(175, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 6;
			this.label1.Text = "Tipo de búsqueda";
			// 
			// tipoBusquedaCB
			// 
			this.tipoBusquedaCB.FormattingEnabled = true;
			this.tipoBusquedaCB.Location = new System.Drawing.Point(175, 22);
			this.tipoBusquedaCB.Name = "tipoBusquedaCB";
			this.tipoBusquedaCB.Size = new System.Drawing.Size(92, 21);
			this.tipoBusquedaCB.TabIndex = 5;
			this.tipoBusquedaCB.SelectedIndexChanged += new System.EventHandler(this.TipoBusquedaCBSelectedIndexChanged);
			// 
			// ordenMovimientoBtn
			// 
			this.ordenMovimientoBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ordenMovimientoBtn.BackgroundImage")));
			this.ordenMovimientoBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.ordenMovimientoBtn.Location = new System.Drawing.Point(133, 7);
			this.ordenMovimientoBtn.Name = "ordenMovimientoBtn";
			this.ordenMovimientoBtn.Size = new System.Drawing.Size(36, 36);
			this.ordenMovimientoBtn.TabIndex = 4;
			this.ordenMovimientoBtn.UseVisualStyleBackColor = true;
			this.ordenMovimientoBtn.Click += new System.EventHandler(this.OrdenMovimientoBtnClick);
			// 
			// guardarPersonajesBtn
			// 
			this.guardarPersonajesBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("guardarPersonajesBtn.BackgroundImage")));
			this.guardarPersonajesBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
			this.guardarPersonajesBtn.Location = new System.Drawing.Point(6, 6);
			this.guardarPersonajesBtn.Name = "guardarPersonajesBtn";
			this.guardarPersonajesBtn.Size = new System.Drawing.Size(36, 36);
			this.guardarPersonajesBtn.TabIndex = 3;
			this.guardarPersonajesBtn.UseVisualStyleBackColor = true;
			this.guardarPersonajesBtn.Click += new System.EventHandler(this.GuardarPersonajesBtnClick);
			// 
			// deletePersonajeBtn
			// 
			this.deletePersonajeBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("deletePersonajeBtn.BackgroundImage")));
			this.deletePersonajeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.deletePersonajeBtn.Location = new System.Drawing.Point(90, 6);
			this.deletePersonajeBtn.Name = "deletePersonajeBtn";
			this.deletePersonajeBtn.Size = new System.Drawing.Size(36, 36);
			this.deletePersonajeBtn.TabIndex = 2;
			this.deletePersonajeBtn.UseVisualStyleBackColor = true;
			this.deletePersonajeBtn.Click += new System.EventHandler(this.DeletePersonajeBtnClick);
			// 
			// personajesPanel
			// 
			this.personajesPanel.AutoScroll = true;
			this.personajesPanel.Location = new System.Drawing.Point(0, 48);
			this.personajesPanel.Name = "personajesPanel";
			this.personajesPanel.Size = new System.Drawing.Size(297, 575);
			this.personajesPanel.TabIndex = 1;
			// 
			// addPersonajeBtn
			// 
			this.addPersonajeBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("addPersonajeBtn.BackgroundImage")));
			this.addPersonajeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.addPersonajeBtn.Location = new System.Drawing.Point(48, 6);
			this.addPersonajeBtn.Name = "addPersonajeBtn";
			this.addPersonajeBtn.Size = new System.Drawing.Size(36, 36);
			this.addPersonajeBtn.TabIndex = 0;
			this.addPersonajeBtn.UseVisualStyleBackColor = true;
			this.addPersonajeBtn.Click += new System.EventHandler(this.AddPersonajeBtnClick);
			// 
			// showTreeView
			// 
			this.showTreeView.Controls.Add(this.treeBusqueda);
			this.showTreeView.Location = new System.Drawing.Point(4, 22);
			this.showTreeView.Name = "showTreeView";
			this.showTreeView.Padding = new System.Windows.Forms.Padding(3);
			this.showTreeView.Size = new System.Drawing.Size(297, 626);
			this.showTreeView.TabIndex = 2;
			this.showTreeView.Text = "Mostrar Arbol";
			this.showTreeView.UseVisualStyleBackColor = true;
			// 
			// treeBusqueda
			// 
			this.treeBusqueda.Location = new System.Drawing.Point(3, 3);
			this.treeBusqueda.Name = "treeBusqueda";
			this.treeBusqueda.Size = new System.Drawing.Size(291, 620);
			this.treeBusqueda.TabIndex = 0;
			// 
			// abrirMapa
			// 
			this.abrirMapa.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("abrirMapa.BackgroundImage")));
			this.abrirMapa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.abrirMapa.Location = new System.Drawing.Point(10, 19);
			this.abrirMapa.Name = "abrirMapa";
			this.abrirMapa.Size = new System.Drawing.Size(36, 36);
			this.abrirMapa.TabIndex = 3;
			this.abrirMapa.UseVisualStyleBackColor = true;
			this.abrirMapa.Click += new System.EventHandler(this.AbrirMapaClick);
			// 
			// columnasPBox
			// 
			this.columnasPBox.Location = new System.Drawing.Point(42, 12);
			this.columnasPBox.Name = "columnasPBox";
			this.columnasPBox.Size = new System.Drawing.Size(740, 23);
			this.columnasPBox.TabIndex = 4;
			this.columnasPBox.TabStop = false;
			// 
			// filasPBox
			// 
			this.filasPBox.Location = new System.Drawing.Point(12, 41);
			this.filasPBox.Name = "filasPBox";
			this.filasPBox.Size = new System.Drawing.Size(24, 670);
			this.filasPBox.TabIndex = 5;
			this.filasPBox.TabStop = false;
			// 
			// infoBtn
			// 
			this.infoBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("infoBtn.BackgroundImage")));
			this.infoBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.infoBtn.Location = new System.Drawing.Point(12, 12);
			this.infoBtn.Name = "infoBtn";
			this.infoBtn.Size = new System.Drawing.Size(24, 23);
			this.infoBtn.TabIndex = 6;
			this.infoBtn.UseVisualStyleBackColor = true;
			// 
			// playBtn
			// 
			this.playBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("playBtn.BackgroundImage")));
			this.playBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.playBtn.Location = new System.Drawing.Point(94, 19);
			this.playBtn.Name = "playBtn";
			this.playBtn.Size = new System.Drawing.Size(36, 36);
			this.playBtn.TabIndex = 7;
			this.playBtn.UseVisualStyleBackColor = true;
			this.playBtn.Click += new System.EventHandler(this.PlayBtnClick);
			// 
			// controlArchivoGB
			// 
			this.controlArchivoGB.Controls.Add(this.limpiartBtn);
			this.controlArchivoGB.Controls.Add(this.playBtn);
			this.controlArchivoGB.Controls.Add(this.abrirMapa);
			this.controlArchivoGB.Location = new System.Drawing.Point(822, 12);
			this.controlArchivoGB.Name = "controlArchivoGB";
			this.controlArchivoGB.Size = new System.Drawing.Size(138, 69);
			this.controlArchivoGB.TabIndex = 8;
			this.controlArchivoGB.TabStop = false;
			this.controlArchivoGB.Text = "Archivo";
			// 
			// limpiartBtn
			// 
			this.limpiartBtn.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("limpiartBtn.BackgroundImage")));
			this.limpiartBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
			this.limpiartBtn.Location = new System.Drawing.Point(52, 19);
			this.limpiartBtn.Name = "limpiartBtn";
			this.limpiartBtn.Size = new System.Drawing.Size(36, 36);
			this.limpiartBtn.TabIndex = 4;
			this.limpiartBtn.UseVisualStyleBackColor = true;
			this.limpiartBtn.Click += new System.EventHandler(this.LimpiartBtnClick);
			// 
			// filasPBox2
			// 
			this.filasPBox2.Location = new System.Drawing.Point(788, 41);
			this.filasPBox2.Name = "filasPBox2";
			this.filasPBox2.Size = new System.Drawing.Size(24, 670);
			this.filasPBox2.TabIndex = 10;
			this.filasPBox2.TabStop = false;
			// 
			// columnasPBox2
			// 
			this.columnasPBox2.Location = new System.Drawing.Point(42, 717);
			this.columnasPBox2.Name = "columnasPBox2";
			this.columnasPBox2.Size = new System.Drawing.Size(740, 23);
			this.columnasPBox2.TabIndex = 11;
			this.columnasPBox2.TabStop = false;
			// 
			// buscarCasillaGP
			// 
			this.buscarCasillaGP.Controls.Add(this.busquedaBTN);
			this.buscarCasillaGP.Controls.Add(this.busquedaTxtBox);
			this.buscarCasillaGP.Location = new System.Drawing.Point(966, 12);
			this.buscarCasillaGP.Name = "buscarCasillaGP";
			this.buscarCasillaGP.Size = new System.Drawing.Size(157, 70);
			this.buscarCasillaGP.TabIndex = 12;
			this.buscarCasillaGP.TabStop = false;
			this.buscarCasillaGP.Text = "Busqueda de casilla";
			// 
			// busquedaBTN
			// 
			this.busquedaBTN.Location = new System.Drawing.Point(6, 41);
			this.busquedaBTN.Name = "busquedaBTN";
			this.busquedaBTN.Size = new System.Drawing.Size(145, 23);
			this.busquedaBTN.TabIndex = 2;
			this.busquedaBTN.Text = "¡Buscar!";
			this.busquedaBTN.UseVisualStyleBackColor = true;
			// 
			// busquedaTxtBox
			// 
			this.busquedaTxtBox.Location = new System.Drawing.Point(6, 19);
			this.busquedaTxtBox.Name = "busquedaTxtBox";
			this.busquedaTxtBox.Size = new System.Drawing.Size(145, 20);
			this.busquedaTxtBox.TabIndex = 1;
			this.busquedaTxtBox.Text = "Ejemplo: A,1";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
			this.ClientSize = new System.Drawing.Size(1135, 747);
			this.Controls.Add(this.buscarCasillaGP);
			this.Controls.Add(this.columnasPBox2);
			this.Controls.Add(this.filasPBox2);
			this.Controls.Add(this.controlArchivoGB);
			this.Controls.Add(this.infoBtn);
			this.Controls.Add(this.filasPBox);
			this.Controls.Add(this.columnasPBox);
			this.Controls.Add(this.opcionesTabControl);
			this.Controls.Add(this.mapaPbox);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "IA Game";
			this.Load += new System.EventHandler(this.MainFormLoad);
			((System.ComponentModel.ISupportInitialize)(this.mapaPbox)).EndInit();
			this.opcionesTabControl.ResumeLayout(false);
			this.texturasTab.ResumeLayout(false);
			this.pesonajesTab.ResumeLayout(false);
			this.showTreeView.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.columnasPBox)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.filasPBox)).EndInit();
			this.controlArchivoGB.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.filasPBox2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.columnasPBox2)).EndInit();
			this.buscarCasillaGP.ResumeLayout(false);
			this.buscarCasillaGP.PerformLayout();
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Button distanciaButton;
		private System.Windows.Forms.ComboBox tipoBusquedaCB;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button ordenMovimientoBtn;
		private System.Windows.Forms.TextBox busquedaTxtBox;
		private System.Windows.Forms.Button busquedaBTN;
		private System.Windows.Forms.GroupBox buscarCasillaGP;
		private System.Windows.Forms.Button guardarPersonajesBtn;
		private System.Windows.Forms.Button guardarTexturasBtn;
		private System.Windows.Forms.PictureBox columnasPBox2;
		private System.Windows.Forms.PictureBox filasPBox2;
		private System.Windows.Forms.Button deletePersonajeBtn;
		private System.Windows.Forms.Button limpiartBtn;
		private System.Windows.Forms.GroupBox controlArchivoGB;
		private System.Windows.Forms.Button playBtn;
		private System.Windows.Forms.Panel personajesPanel;
		private System.Windows.Forms.Panel texturasPanel;
		private System.Windows.Forms.Button infoBtn;
		private System.Windows.Forms.PictureBox filasPBox;
		private System.Windows.Forms.PictureBox columnasPBox;
		private System.Windows.Forms.Button abrirMapa;
		private System.Windows.Forms.Button addPersonajeBtn;
		private System.Windows.Forms.TabPage pesonajesTab;
		private System.Windows.Forms.TabPage texturasTab;
		private System.Windows.Forms.TabControl opcionesTabControl;
		private System.Windows.Forms.PictureBox mapaPbox;
		private System.Windows.Forms.TabPage showTreeView;
		private System.Windows.Forms.TreeView treeBusqueda;
	}
}
