﻿/*
 * Created by SharpDevelop.
 * User: lumar
 * Date: 03/03/2020
 * Time: 09:16 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ia_mapa
{
	/// <summary>
	/// Description of LectorArchivo.
	/// </summary>
	public class LectorArchivo
	{
		// Variables de clase
		private string[] filasMapa;
		private int cantColumnas = 0;
		private int cantFilas = 0;
		private string errores = string.Empty;
		private List<int> listaId = new List<int>();
		private int[,] mapa;
		// Regex para verificar que sean números enteros positivos
		Regex rx = new Regex("^[0-9]+$");
		// Getters
		public int CantColumnas {
			get { return cantColumnas; }
		}
		public int CantFilas {
			get { return cantFilas; }
		}
		public string Errores {
			get { return errores; }
		}
		public List<int> ListaId {
			get { return listaId; }
		}
		public int TotalId{
			get { return listaId.Count; }
		}
		
		public int[,] MapaMatriz(){
			for(int i = 0; i < this.cantFilas; i++) {
				string[] columnas = this.filasMapa[i].Split(',');
				for(int j = 0; j < this.cantColumnas; j++){
					this.mapa[i, j] = Int32.Parse(columnas[j]);
				}
			}
			return this.mapa;
		}
		// Constructor parametrizado
		public LectorArchivo(string[] filasMapa)
		{
			// Guarda el mapa
			this.filasMapa = filasMapa;
			// Usa la primer fila para contar la cantidad de columnas
			// que deberán existir por fila y la cantidad de filas.
			this.cantColumnas = filasMapa[0].Split(',').Length;
			this.cantFilas = filasMapa.Length;
			// Se inicializa la matriz del mapa
			this.mapa = new int[this.cantFilas, this.cantColumnas];
			// Corrobora el contenido del mapa.
			comprobarFilasColumnas();
		}
		// Función que corrobora que el mapa sea n x m
		public bool esValida_NXM()
		{
			return this.cantFilas <= 15 && this.cantColumnas <=15;
		}
		// Función que regresa la cantidad de errores
		public bool sinErrores()
		{
			return this.errores == string.Empty;
		}
		// Función que corrobora que el mapa está escrito correctamente.
		public void comprobarFilasColumnas()
		{
			// Itera por fila
			for(int iFila = 0; iFila < this.filasMapa.Length; iFila++)
			{
				// En caso de existir un espacio dentro de la fila, se elimina.
				this.filasMapa[iFila] = this.filasMapa[iFila].Replace(" ", "");
				// Se toma cada una de las filas leídas
				// Y se separa por "," esperando llegar un número por cada "Split"
				string[] columnas = this.filasMapa[iFila].Split(',');
				// Corrobora que la cantidad de columnas sea igual al de la primer
				// fila que se registró
				if (columnas.Length != this.cantColumnas)
				{
					this.errores += "Fila #" + (iFila + 1) + ": Error en cantidad de columnas.\n";
				}
				// Se itera columna por columna revisando que lo ingresado sea 
				// un número y no un caracter distinto
				int jColumna = 0;
				foreach (string columna in columnas)
				{
					// Revisa si la columna actual es un número entero positivo
					if (!rx.IsMatch(columna))
					{
						this.errores += "Fila #" + (iFila + 1) + " Columna #" + (jColumna + 1) +
							": Error, el carácter no es un Entero Positivo.\n";
					}
					else {
						if(!this.listaId.Contains(int.Parse(columna))){
						   	this.listaId.Add(int.Parse(columna));
						}
					}
					jColumna++;
				}
			}
		}
	}
}
