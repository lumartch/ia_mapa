﻿/*
 * Created by SharpDevelop.
 * User: lumartch
 * Date: 03/03/2020
 * Time: 08:54 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace ia_mapa {
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form {
		// Variables para las filas y columnas
		private int f;
		private int c;
		// Variables para el tamaño de los rectangulos
		private int h;
		private int w;
		// Variables del mapa
		private int[,]mapaMatriz;
		// Diccionario de datos para los Id's, nombre y valor codificado
		private Dictionary<int, string> diccionarioIdsTexturas;
		// Personajes
		private int cantPersonajes;
		// Diccionario para PictureBox y para Textbox de las texturas
		private Dictionary<int, PictureBox> pbDiccionarioTexturas;
		private Dictionary<int, TextBox> txtboxDiccionarioTexturas;
		private bool cambiosTexturas;
		// Diccionario para PictureBox y TextBox de los personajes
		private Dictionary<int, PictureBox> pbDiccionarioPTexturas;
		private Dictionary<int, TextBox> txtDiccionarioPInicio;
		private Dictionary<int, TextBox> txtDiccionarioPFin;
		private Dictionary<int, ComboBox> cbDiccionarioPersonaje;
		// Diccionario de pesos para los personajes
		private int personajeJugable;
		private Dictionary<int, Dictionary<int,float>> personajePesoTerrenos;
		private Dictionary<int, PictureBox> personajePictureBox;
		private int[] x_y_Inicio;
		private int[] x_y_Final;
		// Form auxiliar para mostrar la elección de pesos sobre terrenos y elección de personaje
		private Form formAuxiliar;
		// Tool tip para mostrar la información sobre el mapa
		private ToolTip casillasToolTip = new ToolTip();
		// PictureBox con Fog para la  jugabilidad
		private PictureBox pbFog;
		// Diccionario para el número de visitas por casilla
		private Dictionary<int, string> diccionarioVisitas;
		private int cVisitas;
		// Música de background para la aplicación.
		private SoundPlayer musicaFondo;
		// Variable para el orden de movimiento
		private Dictionary<int, int> ordenMovimiento;
		private Dictionary<int, ComboBox> ordenMovComboB;
		//
		private int medidaOpc;
		public Nodo nodoCola;
		
		public MainForm() {
			// Variables para la cantidad de filas, columnas, personajes, matriz  y dimensiones de los cuadros
			this.f = 0;
			this.c = 0;
			this.h = 0;
			this.w = 0;
			this.cantPersonajes = 0;
			this.mapaMatriz = new int[,]{};
			// Diccionario de id texturas y sus nombres
			this.diccionarioIdsTexturas = new Dictionary<int, string>();
			// Inicialización de los diccionarios
			this.pbDiccionarioTexturas = new Dictionary<int, PictureBox>();
			this.txtboxDiccionarioTexturas = new Dictionary<int, TextBox>();
			this.cambiosTexturas = false;
			// Inicialización de los diccionarios de personajes
			this.pbDiccionarioPTexturas = new Dictionary<int, PictureBox>();
			this.txtDiccionarioPInicio = new Dictionary<int, TextBox>();
			this.txtDiccionarioPFin = new Dictionary<int, TextBox>();
			this.cbDiccionarioPersonaje = new Dictionary<int, ComboBox>();
			// Inicialización de pesos para los personajes
			this.personajeJugable = 0;
			this.personajePesoTerrenos = new Dictionary<int, Dictionary<int, float>>();
			this.personajePictureBox = new Dictionary<int, PictureBox>();
			// Se inicializa los puntos X y Y para el personaje jugable
			this.x_y_Inicio = new int[2];
			this.x_y_Final = new int[2];
			// Inicializa el diccionario de visistas
			this.diccionarioVisitas = new Dictionary<int, string>();
			// Inicializa el diccionario de orden de movimiento
			this.ordenMovimiento = new Dictionary<int, int>();
			this.ordenMovimiento.Add(0, 0);
			this.ordenMovimiento.Add(1, 1);
			this.ordenMovimiento.Add(2, 2);
			this.ordenMovimiento.Add(3, 3);
			this.ordenMovComboB = new Dictionary<int, ComboBox>();
			InitializeComponent();
			// Se evita que la pantalla se maximise y el tab de desactiva 
			this.MaximizeBox = false;
			this.opcionesTabControl.Enabled = false;
			this.infoBtn.Enabled = false;
			this.buscarCasillaGP.Enabled = false;
			this.playBtn.Enabled = false;
			this.abrirMapa.Focus();
			// Se crean los ToolTips necesarios para los botones
		    ToolTip botonesPrincipalesTP = new ToolTip(); 
	        botonesPrincipalesTP.Active = true; 
	        botonesPrincipalesTP.AutoPopDelay = 4000; 
	        botonesPrincipalesTP.InitialDelay = 600; 
	        //botonesPrincipalesTP.ToolTipIcon = ToolTipIcon.Info;
	        botonesPrincipalesTP.SetToolTip(this.abrirMapa, "Abre el mapa desde un directorio externo.");
	        botonesPrincipalesTP.SetToolTip(this.guardarTexturasBtn, "Guarda la configuración actual de los terrenos.");
	        botonesPrincipalesTP.SetToolTip(this.limpiartBtn, "Se eliminan todas las configuraciones, incluyendo el mapa.");
	        botonesPrincipalesTP.SetToolTip(this.guardarPersonajesBtn, "Guarda la configuración actual de los personajes.");
	        botonesPrincipalesTP.SetToolTip(this.deletePersonajeBtn, "Elimina al último personaje.");
	        botonesPrincipalesTP.SetToolTip(this.addPersonajeBtn, "Agrega un nuevo personaje.");	
	        botonesPrincipalesTP.SetToolTip(this.ordenMovimientoBtn, "Configura el orden de movimiento para los personajes");
	        //
	        // Inicializa los tipos de búsqueda
			this.tipoBusquedaCB.Items.Insert(0, "Manual");
        	this.tipoBusquedaCB.Items.Insert(1, "Profundidad");
        	this.tipoBusquedaCB.Items.Insert(2, "Coste Uniforme");
        	this.tipoBusquedaCB.Items.Insert(3, "Primero voraz");
        	this.tipoBusquedaCB.Items.Insert(4, "A*");
        	this.tipoBusquedaCB.DropDownStyle = ComboBoxStyle.DropDownList;
        	this.tipoBusquedaCB.SelectedIndex = 0;
			this.distanciaButton.Enabled = false;
			
			this.medidaOpc = 0;
	        // Agrega el evento para los tabs
	        this.opcionesTabControl.Selecting += new TabControlCancelEventHandler(CambioTab);
	        // Asigna el tool tip correspondiente al mapa
		    this.casillasToolTip.ReshowDelay = 500;
			this.casillasToolTip.IsBalloon = true;
			this.casillasToolTip.SetToolTip(this.mapaPbox, "");
			// Eventos para el mapa
			this.mapaPbox.MouseDown += new MouseEventHandler(CasillaInfo);
			this.mapaPbox.MouseLeave += new EventHandler(CasillaInfoClear);
			// Evento de la casilla de busqueda
			this.busquedaTxtBox.GotFocus += new EventHandler(RemovePlaceHolderCoordenadas);
			this.busquedaTxtBox.LostFocus += new EventHandler(PutPlaceHolderCoordenadasBusqueda);
			this.busquedaTxtBox.ForeColor = Color.FromArgb(89, 89, 89);
			this.busquedaTxtBox.Font = new Font(this.busquedaTxtBox.Font, FontStyle.Italic);
			this.busquedaTxtBox.TextChanged += new EventHandler(TextRegexBusqueda);
			// Crea la variable que va a reproducir la música de fondo
			this.musicaFondo = new SoundPlayer(Directory.GetCurrentDirectory() + "\\sounds\\title.wav");
			this.musicaFondo.PlayLooping();
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			// Se desactiva el tab con los controles para manejar las texturas y los personajes
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
		
		}
		
		// Función para abrir el cuadro de dialogo.
		private bool leerArchivo() {
			// Crea una instancia para buscar el archivo
			using (OpenFileDialog openFileDialog = new OpenFileDialog()) {
				// Se define la clase de archivo que se busca.
				openFileDialog.Filter = "Archivos de texto (*.txt)|*.txt";
				openFileDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Mapas";
				if (openFileDialog.ShowDialog() == DialogResult.OK) {
					// Si el archivo es encontrado, se leen las líneas dentro de él
					string[] contenidoArchivo = File.ReadAllLines(openFileDialog.FileName);
					// Se crea la instancia para corroborar la integridad 
					// Del archivo en cuestión
					if(new FileInfo(openFileDialog.FileName).Length == 0) {
						MessageBox.Show("Se ha ingresado un archivo vacío.\n" +
						                "Esto no afectará el uso del programa, pero si desea cargar un mapa" +
						                " este deberá contener información.", "¡Archivo vacío!", MessageBoxButtons.OK,MessageBoxIcon.Warning);
						return false;
					}
					LectorArchivo rFile = new LectorArchivo(contenidoArchivo);
					if (rFile.sinErrores() && rFile.esValida_NXM() && rFile.TotalId <= 10) {
						MessageBox.Show("El archivo fue leído exitosamente, puede continuar.", "¡Archivo correcto! :D", MessageBoxButtons.OK, MessageBoxIcon.Information);
						// Si existen datos previos, se vacian
						if(this.diccionarioIdsTexturas.Count != 0) {
							liberarMemoria();
						}
						// Se asigna el valor de las filas y las columnas
						this.f = rFile.CantFilas;
						this.c = rFile.CantColumnas;
						// Se crea una copia del mapa para recorrer
						this.mapaMatriz = rFile.MapaMatriz();
						// Se rellena el diccionario de datos con los Id's recolectados del mapa
						foreach (int id in rFile.ListaId){
							this.diccionarioIdsTexturas.Add(id, "Sin textura");
						}
					}
					else {
						// Revisa la cantidad de Id's posibles a ingresar.
						if(rFile.TotalId > 10){
							MessageBox.Show("Excedente de Id's, solo puede existir como máximo 10 Id's." +
							                "\nUsted ha ingresado " + rFile.TotalId + " lo cual sobre pasa por " +
							                (rFile.TotalId - 10) + " el límite máximo.", "¡Desbordamiento de Id's!",
							                MessageBoxButtons.OK,MessageBoxIcon.Warning);
							return false;
						}
						// Se valida el tamaño N X M
						if (!rFile.esValida_NXM()) {
							// Se imprime donde el archivo ha fallado en especificaciones
							if (rFile.CantFilas > 15) {
								MessageBox.Show("Las filas son mayores a 15.", "¡Desbordamiento de Filas!", 
								                MessageBoxButtons.OK,MessageBoxIcon.Warning);
								return false;
							}
							if (rFile.CantColumnas > 15) {
								MessageBox.Show("Las columnas son mayores a 15.", "¡Desbordamiento de Columnas!",
								                MessageBoxButtons.OK,MessageBoxIcon.Warning);
								return false;
							}
						}
						else {
							// Si tiene errores el archivo lo revisa
							if (!rFile.sinErrores()) {
								MessageBox.Show(rFile.Errores, "¡Errores en el archivo!", 
								                MessageBoxButtons.OK,MessageBoxIcon.Hand);
								return false;
							}
						}
					}
				}
				else {
					return false;
				}
			}
			return true;
		}
		// Evento para regresar la información de la casilla por el evento MouseDown
		private void CasillaInfo(Object sender, MouseEventArgs e){
			if(e.X > this.w*this.c || e.Y > this.h*this.f){
				return;	
			}
			this.casillasToolTip.SetToolTip(this.mapaPbox, "(" + Convert.ToChar((e.X/this.w) + 65).ToString()+
		              "," + ((e.Y/this.h) + 1) + "): " +  this.diccionarioIdsTexturas[this.mapaMatriz[e.Y/this.h, e.X/this.w]].ToString());
		}
		// Limpia el tooltip dedicado a la información de la casilla
		private void CasillaInfoClear(Object sender, EventArgs e){
			this.casillasToolTip.SetToolTip(this.mapaPbox, "");
		}
		// Crea evento que escucha si las tabs cambian de posición
		private void CambioTab(Object sender, TabControlCancelEventArgs  e){
			// Valida si los datos han terminado de ingresarse en el tab de texturas
			TabPage current = (sender as TabControl).SelectedTab;
			if(this.diccionarioIdsTexturas.Count != 0){
				if(current != this.texturasTab){
					if(datosCompletosTexturas()){
						if(this.cambiosTexturas == true){
							insertarTexturas();	
							this.cambiosTexturas = false;
						}
						if(this.cantPersonajes == 0){
							crearPersonaje();	
							FormOrdenMovimiento();
						}
					} else {
						e.Cancel = true;
					}	
				}
				
				if(current != this.pesonajesTab){
					if(datosCompletosPersonajes() && this.cambiosTexturas == false){
						this.playBtn.Focus();
						// ToolTip para indicarle al usuario que hacer
						ToolTip indicaciones = new ToolTip();
						indicaciones.IsBalloon = true;
						indicaciones.AutoPopDelay = 4000; 
						indicaciones.InitialDelay = 600; 
						indicaciones.IsBalloon = true; 
						indicaciones.ToolTipIcon = ToolTipIcon.Info;
						indicaciones.Show("Ahora con todos los datos guardados, " +
						                  "\npuede empezar a jugar :D", this.playBtn, 0, -90, 10000);
					}
					else{
						e.Cancel = true;
					}
				}
			}

		}
		// Libera la memoria de todos los datos pasados
		private void liberarMemoria(){
			// Vacía el mapa
			Array.Clear(this.mapaMatriz, 0, this.mapaMatriz.Length);
			this.f = 0;
			this.c = 0;
			this.h = 0;
			this.w = 0;
			// Carga el recuadro del mapa si fue leído correctamente el archivo
			this.mapaPbox.Image = new Bitmap(this.mapaPbox.Width,this.mapaPbox.Height);
			this.filasPBox.Image = new Bitmap(this.filasPBox.Width,this.filasPBox.Height);
			this.filasPBox2.Image = this.filasPBox.Image;
			this.columnasPBox.Image = new Bitmap(this.columnasPBox.Width,this.columnasPBox.Height);
			this.columnasPBox2.Image = this.columnasPBox.Image;
			this.cambiosTexturas = false;
			// Vacía todos los Id's Ingresados en el diccionario
			this.diccionarioIdsTexturas.Clear();
			// Vacía todos los elementos dentro del panel de texturas.
			this.texturasPanel.Controls.Clear();
			// Se libera el espacio de memoria de los diccionarios
			this.pbDiccionarioTexturas.Clear();
			this.txtboxDiccionarioTexturas.Clear();
			// Vacía todos los personajes 
			this.cantPersonajes = 0;
			this.personajesPanel.Controls.Clear();
			this.cbDiccionarioPersonaje.Clear();
			this.txtDiccionarioPFin.Clear();
			this.txtDiccionarioPInicio.Clear();
			this.pbDiccionarioPTexturas.Clear();
			// Se vacían los pesos de los personajes
			this.personajePesoTerrenos.Clear();
			// Eliminar todos los PictureBox
			this.personajePictureBox.Clear();
			// Elimina el árbol anterior
			this.treeBusqueda.Nodes.Clear();
			// Regresa a la TAB de texturas
			this.opcionesTabControl.SelectTab(0);
			this.diccionarioVisitas.Clear();
		}
		// Función que llena los formularios del TabTexturas
		private void rellenarTabTexturas(){
			int i = 0;
			foreach(KeyValuePair<int, string> entry in this.diccionarioIdsTexturas) {
			    // Se crea el Group Box para cada textura
			    GroupBox gp = new GroupBox();
			    gp.SetBounds(5,0 + i, this.texturasPanel.Width - 25, 90);
			    gp.Name = "groupBoxId" + entry.Key;
			    gp.Text ="Textura ID: " + entry.Key;
			    gp.Parent = this.texturasPanel;
			    // ToolTip para los elementos de las texturas
			    ToolTip texturaToolTip = new ToolTip(); 
		        texturaToolTip.Active = true; 
		        texturaToolTip.AutoPopDelay = 4000; 
		        texturaToolTip.InitialDelay = 600; 
		        texturaToolTip.ToolTipIcon = ToolTipIcon.Info; 
			    // Se crea el Picture Box que contendrá la textura en minuatura
			    PictureBox picBox = new PictureBox();
			    picBox.SetBounds(15, 20, 60,60);
			    picBox.Name = "pictureBoxId" + entry.Key;
			    picBox.BorderStyle = BorderStyle.FixedSingle;
				picBox.SizeMode = PictureBoxSizeMode.StretchImage;
				picBox.BackColor = Color.White;
			    picBox.Parent = gp;
			    this.pbDiccionarioTexturas.Add(entry.Key, picBox);
		        texturaToolTip.SetToolTip(picBox, "Vista previa de la textura.");
			    // Se crea el TextBox para el nombre de la textura
			    TextBox txtbx = new TextBox();
			    txtbx.SetBounds(85, 60, 170, 20);
			    txtbx.ForeColor = Color.FromArgb(89, 89, 89);
			    txtbx.Name = "textBoxId" + entry.Key;
			    txtbx.Text = "Nombre del terreno";
			    txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
			    txtbx.BorderStyle = BorderStyle.FixedSingle; 
			    txtbx.GotFocus += new EventHandler(RemovePlaceHolder);
			    txtbx.LostFocus += new EventHandler(PutPlaceHolder);
			    txtbx.TextChanged += new EventHandler(RegularText);
			    txtbx.Parent = gp;
			    this.txtboxDiccionarioTexturas.Add(entry.Key, txtbx);
			    // Se crea un ToolTip Para el textbox
		        texturaToolTip.SetToolTip(txtbx, "Este será el nombre con el que " +
		                         "\nse identificará el terreno.");
			    // Se crea el Botón para aplicar la textura
			    Button btn = new Button();
			    btn.SetBounds(85, 20, 35,35);
			    btn.Name = "btnTextId" + entry.Key;
			    string path = Directory.GetCurrentDirectory() + "\\icons\\add.png";
			    btn.BackgroundImage = Image.FromFile(path);
			    btn.BackgroundImageLayout = ImageLayout.Stretch;
			    btn.Click += new EventHandler(AgregarTextura);
			    btn.Parent = gp;
		        texturaToolTip.SetToolTip(btn, "Buscar textura en directorio.");
			    // Se crea botón para el ColorPalett
			    Button btn_2 = new Button();
			    btn_2.SetBounds(120, 20, 35,35);
			    btn_2.Name = "btnPalId" + entry.Key;
			    path = path.Replace("add.png","color_palette.png");
			    btn_2.BackgroundImage = Image.FromFile(path);
			    btn_2.BackgroundImageLayout = ImageLayout.Stretch;
			    btn_2.Click += new EventHandler(ColorPalette);
			    btn_2.Parent = gp;
		        texturaToolTip.SetToolTip(btn_2, "Crear textura desde Color Palette.");
			    // Se crea botón para limpiar los datos ingresados en la textura
			    Button btn_3 = new Button();
			    btn_3.SetBounds(155, 20, 35,35);
			    btn_3.Name = "btnDelId" + entry.Key;
			    path = path.Replace("color_palette.png","delete.png");
			    btn_3.BackgroundImage = Image.FromFile(path);
			    btn_3.BackgroundImageLayout = ImageLayout.Stretch;
			    btn_3.Click += new EventHandler(LimparDatosTextura);
			    btn_3.Parent = gp;
		        texturaToolTip.SetToolTip(btn_3, "Limpiar todos los campos de la textura actual.");
			    i += 100;
			}
		}
		// Se crea el evento ColorPalette, para mostrar el elemento del mismo nombre
		private void ColorPalette(object sender, System.EventArgs e) { 
			ColorDialog d = new ColorDialog();
			Button btn = (Button)sender;
			if(d.ShowDialog() == DialogResult.OK){
				string id = btn.Name;
				id = id.Replace("btnPalId","");
				// Recupera el valor por medio del diccionario de datos
		        PictureBox pb = this.pbDiccionarioTexturas[Int32.Parse(id)];
		        pb.Image = null;
				pb.BackColor = d.Color;
				this.cambiosTexturas = true;
			}  
		} 
		// Función para quitar el PlaceHolder para el nombre de las texturas
		private void RemovePlaceHolder(Object sender, EventArgs e) {
			TextBox txtbx = ((TextBox)sender);
			if(txtbx.Text == "Nombre del terreno"){
				txtbx.ForeColor = Color.Black;
				txtbx.Font = new Font(txtbx.Font, FontStyle.Regular);
				txtbx.Text = "";
			}
		}
		// Función para colocar el PlaceHolder para el nombre de las texturas
		private void PutPlaceHolder(Object sender, EventArgs e) {
			TextBox txtbx = ((TextBox)sender);
			if (string.IsNullOrWhiteSpace(txtbx.Text)){
				txtbx.ForeColor = Color.FromArgb(89, 89, 89);
				txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
				txtbx.Text = "Nombre del terreno";
			}
		}
		
		// Función para quitar el PlaceHolder de las coordenadas
		private void RemovePlaceHolderCoordenadas(Object sender, EventArgs e) {
			TextBox txtbx = ((TextBox)sender);
			if(txtbx.Text ==  "Ejemplo: A,1"){
				txtbx.ForeColor = Color.Black;
				txtbx.Font = new Font(txtbx.Font, FontStyle.Regular);
				txtbx.Text = "";
			}
		}
		// Función para colocar el PlaceHolder de las coordenadas
		private void PutPlaceHolderCoordenadas(Object sender, EventArgs e) {
			TextBox txtbx = ((TextBox)sender);
			if (string.IsNullOrWhiteSpace(txtbx.Text)){
				txtbx.ForeColor = Color.FromArgb(89, 89, 89);
				txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
				txtbx.BackColor = Color.Empty;
				txtbx.Text = "Ejemplo: A,1";
				this.playBtn.Enabled = false;
			}
		}
		// Función para colocar el PlaceHolder de las coordenadas
		private void PutPlaceHolderCoordenadasBusqueda(Object sender, EventArgs e) {
			TextBox txtbx = ((TextBox)sender);
			if (string.IsNullOrWhiteSpace(txtbx.Text)){
				txtbx.ForeColor = Color.FromArgb(89, 89, 89);
				txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
				txtbx.BackColor = Color.Empty;
				txtbx.Text = "Ejemplo: A,1";
			}
		}
		// Evento para corroborar que la expresión regular de la búsqueda sea el correcto
		private void TextRegexBusqueda(Object sender,EventArgs e){
			TextBox txtbx = ((TextBox)sender);
			// Se verifica que el REGEX para las coordenadas son correctas.
			if ((new Regex("^[A-Z],[0-9]+$")).IsMatch(txtbx.Text) || txtbx.Text == "Ejemplo: A,1"){
				txtbx.BackColor = Color.Empty;
			}
			else {
				txtbx.BackColor = Color.LightSkyBlue;
			}
		}
		// Evento para corroborar que la expresión regular de las coordenadas sea el correcto
		private void TextRegex(Object sender,EventArgs e){
			TextBox txtbx = ((TextBox)sender);
			// Se verifica que el REGEX para las coordenadas son correctas.
			if ((new Regex("^[A-Z],[0-9]+$")).IsMatch(txtbx.Text) || txtbx.Text == "Ejemplo: A,1"){
				txtbx.BackColor = Color.Empty;
			}
			else {
				txtbx.BackColor = Color.LightSkyBlue;
				this.playBtn.Enabled = false;
			}
		}
		// Función para colorear cuando los TextBox de las texturas estén vacíos
		private void RegularText(Object sender, EventArgs e){
			TextBox txtbx = ((TextBox)sender);
			if (!string.IsNullOrWhiteSpace(txtbx.Text)){
				txtbx.BackColor = Color.Empty;
			}
			else {
				txtbx.BackColor = Color.LightSkyBlue;
			}
		}
		// Función para agregar una imagen a un PictureBox
		private void AgregarTextura(Object sender, EventArgs e){
			Button btn = (Button)sender;
			var filePath = string.Empty;
			// Función para abrir un FileDialog
			using (OpenFileDialog openFileDialog = new OpenFileDialog()) {
			    openFileDialog.Filter = "PNG (*.png)|*.png|JPG (*.jpg)|*.jpg|JPEG (*.jpeg)|*.jpeg";
			    openFileDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\texturas_terrenos\\";
			    if (openFileDialog.ShowDialog() == DialogResult.OK) {
			        filePath = openFileDialog.FileName;
			        string id = btn.Name;
			        id = id.Replace("btnTextId","");
			        // Recupera el valor por medio del diccionario de datos
			        PictureBox pb = this.pbDiccionarioTexturas[Int32.Parse(id)];
			        pb.BackColor = Color.Transparent;
					pb.Image = Image.FromFile(filePath);
					this.cambiosTexturas = true;
					// Se ingresan los datos directamente en el TextBox
					this.txtboxDiccionarioTexturas[Int32.Parse(id)].Text = (Path.GetFileNameWithoutExtension(openFileDialog.FileName));
					this.txtboxDiccionarioTexturas[Int32.Parse(id)].ForeColor = Color.Black;
					this.txtboxDiccionarioTexturas[Int32.Parse(id)].Font = new Font(this.txtboxDiccionarioTexturas[Int32.Parse(id)].Font, FontStyle.Regular);
			    }
			}
		}
		
		// Función que verifica que los datos esten completos para colocar las texturas pertinentes
		private bool datosCompletosTexturas(){
			foreach(KeyValuePair<int, TextBox> entry in this.txtboxDiccionarioTexturas){
				if(entry.Value.Text == "Nombre del terreno"){
					ToolTip tt = new ToolTip();
        			tt.Show("Ingrese el nombre del terreno por favor.", entry.Value, 2000);
					entry.Value.Focus();
					return false;
				}
				else{
					this.diccionarioIdsTexturas[entry.Key] = entry.Value.Text;
				}
			}
			return true;
		}
		// Función que verifica que esten todos los datos de los personajes completos y correctos
		private bool datosCompletosPersonajes(){
			// Verifica que esten completas las coordenadas de inicio
			foreach(KeyValuePair<int, TextBox> entry in this.txtDiccionarioPInicio){
				if(entry.Value.Text == "Ejemplo: A,1" || !(new Regex("^[A-Z]+,[0-9]+$")).IsMatch(entry.Value.Text)){
					ToolTip tt = new ToolTip();
        			tt.Show("Ingrese la coordenada por favor.", entry.Value, 2000);
					entry.Value.Focus();
					return false;
				}
				string [] coordenadas = entry.Value.Text.Split(',');
				int columna = ((int)char.Parse(coordenadas[0]) - 65);
				int fila = (Int32.Parse(coordenadas[1]));
				// Verifica que las coordenadas de inicio estén correctas
				if(columna > this.c - 1){
					ToolTip t_Tip = new ToolTip();
					t_Tip.Active = true; 
					t_Tip.AutoPopDelay = 4000; 
					t_Tip.InitialDelay = 600; 
					t_Tip.IsBalloon = true; 
					t_Tip.ToolTipIcon = ToolTipIcon.Info; 
					t_Tip.Show("La columna fuera de los límites del mapa." +
					           "\nIntente una nueva coordenada.", entry.Value, 0, -90, 2000);
					entry.Value.Focus();
					return false;
				}
				if(fila > this.f || fila < 1){
					ToolTip t_Tip = new ToolTip();
					t_Tip.Active = true; 
					t_Tip.AutoPopDelay = 4000; 
					t_Tip.InitialDelay = 600; 
					t_Tip.IsBalloon = true; 
					t_Tip.ToolTipIcon = ToolTipIcon.Info; 
					t_Tip.Show("La fila fuera de los límites del mapa." +
					           "\nIntente una nueva coordenada.", entry.Value, 0, -90, 2000);
					entry.Value.Focus();
					return false;
				}
			}
			// Verifica que esten completas las coordenadas de Fin
			foreach(KeyValuePair<int, TextBox> entry in this.txtDiccionarioPFin){
				if(entry.Value.Text == "Ejemplo: A,1" || !(new Regex("^[A-Z]+,[0-9]+$")).IsMatch(entry.Value.Text)){
					ToolTip tt = new ToolTip();
        			tt.Show("Ingrese la coordenada por favor.", entry.Value, 2000);
					entry.Value.Focus();
					return false;
				}
				string [] coordenadas = entry.Value.Text.Split(',');
				int columna = ((int)char.Parse(coordenadas[0]) - 65);
				int fila = (Int32.Parse(coordenadas[1]));
				if(columna > this.c - 1 ){
					ToolTip t_Tip = new ToolTip();
					t_Tip.Active = true; 
					t_Tip.AutoPopDelay = 4000; 
					t_Tip.InitialDelay = 600; 
					t_Tip.IsBalloon = true; 
					t_Tip.ToolTipIcon = ToolTipIcon.Info; 
					t_Tip.Show("La columna fuera de los límites del mapa." +
					           "\nIntente una nueva coordenada.", entry.Value, 0, -90, 2000);
					entry.Value.Focus();
					return false;
				}
				if(fila > this.f || fila < 1){
					ToolTip t_Tip = new ToolTip();
					t_Tip.Active = true; 
					t_Tip.AutoPopDelay = 4000; 
					t_Tip.InitialDelay = 600; 
					t_Tip.IsBalloon = true; 
					t_Tip.ToolTipIcon = ToolTipIcon.Info; 
					t_Tip.Show("La fila fuera de los límites del mapa." +
					           "\nIntente una nueva coordenada.", entry.Value, 0, -90, 2000);
					entry.Value.Focus();
					return false;
				}
			}
			
			// Verifica las coordenadas para cada uno de los Personajes
			foreach(KeyValuePair<int, Dictionary<int, float>> entry in this.personajePesoTerrenos){
					string [] coordenadas = this.txtDiccionarioPInicio[entry.Key].Text.Split(',');
					int columna = ((int)char.Parse(coordenadas[0]) - 65);
					int fila = (Int32.Parse(coordenadas[1]) - 1);
					// Verifica las corrdenadas para la posición de inicio
					if(entry.Value[this.mapaMatriz[fila, columna]] < 0){
						ToolTip t_Tip = new ToolTip();
						t_Tip.Active = true; 
						t_Tip.AutoPopDelay = 4000; 
						t_Tip.InitialDelay = 600; 
						t_Tip.IsBalloon = true; 
						t_Tip.ToolTipIcon = ToolTipIcon.Info; 
						t_Tip.Show("Es imposible colocar al personaje allí." +
						           "\nIntente una nueva coordenada.", this.txtDiccionarioPInicio[entry.Key], 
						           0, -90, 2000);
						this.txtDiccionarioPInicio[entry.Key].Focus();
						return false;
					}
					
					// Verifica las corrdenadas para la posición final
					coordenadas = this.txtDiccionarioPFin[entry.Key].Text.Split(',');
					columna = ((int)char.Parse(coordenadas[0]) - 65);
					fila = (Int32.Parse(coordenadas[1]) - 1);
					if(entry.Value[this.mapaMatriz[fila, columna]] < 0){
						ToolTip t_Tip = new ToolTip();
						t_Tip.Active = true; 
						t_Tip.AutoPopDelay = 4000; 
						t_Tip.InitialDelay = 600; 
						t_Tip.IsBalloon = true; 
						t_Tip.ToolTipIcon = ToolTipIcon.Info; 
						t_Tip.Show("Es imposible que el personaje llegue a esa casilla." +
						           "\nIntente una nueva coordenada.", this.txtDiccionarioPFin[entry.Key], 
						           0, -90, 2000);
						this.txtDiccionarioPFin[entry.Key].Focus();
						return false;
					}
				}
			return true;
		}
		// Función que elimina todos los datos ingresados a una textura
		private void LimparDatosTextura(Object sender, EventArgs e){
			Button btn = (Button)sender;
			string id = btn.Name;
	        id = id.Replace("btnDelId","");
	        // Recupera el valor por medio del diccionario de datos y vacía al PictureBox
	        PictureBox pb = this.pbDiccionarioTexturas[Int32.Parse(id)];
	        pb.Image = null;
			pb.BackColor = Color.Transparent;
	        // Recupera el valor por medio del diccionario de datos y vacía al TextBox
			TextBox txtbx = this.txtboxDiccionarioTexturas[Int32.Parse(id)];
			txtbx.ForeColor = Color.FromArgb(89, 89, 89);
			txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
			txtbx.Text = "Nombre del terreno";
		}
		// Evento para Agregar un personaje
		void AddPersonajeBtnClick(object sender, EventArgs e) {
			crearPersonaje();
		}
		// Función que crea el formulario del personaje
		private void crearPersonaje(){
			if(this.cantPersonajes < 5){
				this.cantPersonajes++;
				GroupBox gp = new GroupBox();
				gp.SetBounds(3, ((this.cantPersonajes-1) * 100), 288, 90);
				gp.Name = "personajeGB" + this.cantPersonajes;
				gp.Text ="Personaje " + this.cantPersonajes;
			    gp.Parent = this.personajesPanel;
			    // ToolTip para los elementos de los personajes
			    ToolTip personajeToolTip = new ToolTip(); 
		        personajeToolTip.Active = true; 
		        personajeToolTip.AutoPopDelay = 4000; 
		        personajeToolTip.InitialDelay = 600; 
		        personajeToolTip.ToolTipIcon = ToolTipIcon.Info; 
		        // Se crea el Picture Box que contendrá el personaje en minuatura
			    PictureBox picBox = new PictureBox();
			    picBox.SetBounds(7, 19, 60,60);
			    picBox.Name = "personajePB" + this.cantPersonajes;
			    picBox.BorderStyle = BorderStyle.FixedSingle;
				picBox.SizeMode = PictureBoxSizeMode.StretchImage;
				string path = Directory.GetCurrentDirectory() + "\\characters\\";
				switch(this.cantPersonajes - 1){
					case 0:
						path += "green.png";
						break;
					case 1:
						path += "red.png";
						break;
					case 2:
						path += "purple.png";
						break;
					case 3:
						path += "blue.png";
						break;
					case 4:
						path += "black.png";
						break;
					default:
						break;
				}
				picBox.Image = Image.FromFile(path);
			    picBox.Parent = gp;
			    picBox.BackColor = Color.FromArgb(26,26,26);
	        	personajeToolTip.SetToolTip(picBox, "Vista previa del personaje.");
	        	this.pbDiccionarioPTexturas.Add(this.cantPersonajes, picBox);
	        	// Combobox para elegir al personaje.
	        	ComboBox cb = new ComboBox();
	        	cb.SetBounds(73,19,170,23);
	        	cb.Name = "personajeCB" + this.cantPersonajes;
	        	cb.Items.Insert(0, "Green");
	        	cb.Items.Insert(1, "Red");
	        	cb.Items.Insert(2, "Purple");
	        	cb.Items.Insert(3, "Blue");
	        	cb.Items.Insert(4, "Black");
	        	cb.DropDownStyle = ComboBoxStyle.DropDownList;
	        	cb.SelectedIndex = this.cantPersonajes - 1;
	        	cb.SelectedIndexChanged += new EventHandler(cambiarPersonaje);
	        	cb.Parent = gp;
	        	personajeToolTip.SetToolTip(cb, "Elección de personaje.");
	        	this.cbDiccionarioPersonaje.Add(this.cantPersonajes, cb);
	        	// Button para cambiar costos de movimiento
	        	Button btn = new Button();
	        	btn.Name = "personajeConfBtn" + this.cantPersonajes;
	        	btn.SetBounds(253,18,23,23);
	        	path = Directory.GetCurrentDirectory() + "\\icons\\config.png";
	        	btn.BackgroundImage = Image.FromFile(path);
	        	btn.BackgroundImageLayout = ImageLayout.Stretch;
	        	btn.Click += new EventHandler(ModificarCostos);
	        	btn.Parent = gp;
	        	personajeToolTip.SetToolTip(btn, "Configuración de los costos de movimiento.");
			    // Se crea el TextBox para el estado inicial
			    TextBox txtbx = new TextBox();
			    txtbx.SetBounds(73,59,100,20);
			    txtbx.ForeColor = Color.FromArgb(89, 89, 89);
			    txtbx.Name = "personajeInicioTxt"+ this.cantPersonajes;
			    txtbx.Text = "Ejemplo: A,1";
			    txtbx.Font = new Font(txtbx.Font, FontStyle.Italic);
			    txtbx.BorderStyle = BorderStyle.FixedSingle; 
			    txtbx.GotFocus += new EventHandler(RemovePlaceHolderCoordenadas);
			    txtbx.LostFocus += new EventHandler(PutPlaceHolderCoordenadas);
			    txtbx.TextChanged += new EventHandler(TextRegex);
			    txtbx.Parent = gp;
	        	personajeToolTip.SetToolTip(txtbx, "Estado inicial del personaje.");
	        	this.txtDiccionarioPInicio.Add(this.cantPersonajes, txtbx);
			     // Se crea el TextBox para el estado final
			    TextBox txtbx_1 = new TextBox();
			    txtbx_1.SetBounds(179,59,100,20);
			    txtbx_1.ForeColor = Color.FromArgb(89, 89, 89);
			    txtbx_1.Name = "personajeFinTxt" + this.cantPersonajes;
			    txtbx_1.Text = "Ejemplo: A,1";
			    txtbx_1.Font = new Font(txtbx.Font, FontStyle.Italic);
			    txtbx_1.BorderStyle = BorderStyle.FixedSingle; 
			    txtbx_1.GotFocus += new EventHandler(RemovePlaceHolderCoordenadas);
			    txtbx_1.LostFocus += new EventHandler(PutPlaceHolderCoordenadas);
			    txtbx_1.TextChanged += new EventHandler(TextRegex);
			    txtbx_1.Parent = gp;
	        	personajeToolTip.SetToolTip(txtbx_1, "Estado meta/final del personaje.");
	        	this.txtDiccionarioPFin.Add(this.cantPersonajes, txtbx_1);
	        	// Se agrega en el diccionario el personaje con los pesos correspondientes.
	        	Dictionary<int, float> dPesos = new Dictionary<int, float>();
	        	foreach(KeyValuePair<int, string> entry in this.diccionarioIdsTexturas){
	        		dPesos.Add(entry.Key, 0);
	        	}
	        	this.personajePesoTerrenos.Add(this.cantPersonajes, dPesos);
	        	// Se crea el PictureBox para el personaje
	        	PictureBox pbPersonaje = new PictureBox();
	        	pbPersonaje.SetBounds(0, 0, this.w, this.h);
	        	pbPersonaje.BackColor = Color.Transparent;
	        	pbPersonaje.SizeMode = PictureBoxSizeMode.StretchImage;
	        	pbPersonaje.Parent = this.mapaPbox;
	        	pbPersonaje.Hide();
	        	this.personajePictureBox.Add(this.cantPersonajes, pbPersonaje);
	        	crearFormCostos(this.cantPersonajes);
			}
		}
		
		// Evento para pintar el personaje deseado en el PictureBox
		private void cambiarPersonaje(Object sender, EventArgs e){
			string path = Directory.GetCurrentDirectory() + "\\characters\\";
			ComboBox cb = (ComboBox)sender;
			switch(cb.SelectedIndex){
				case 0:
					path += "green.png";
					break;
				case 1:
					path += "red.png";
					break;
				case 2:
					path += "purple.png";
					break;
				case 3:
					path += "blue.png";
					break;
				case 4:
					path += "black.png";
					break;
				default:
					break;
			}
			string id = cb.Name.Replace("personajeCB", "");
			this.pbDiccionarioPTexturas[Int32.Parse(id)].Image = Image.FromFile(path);
		}
		// Evento para Eliminar un personaje
		void DeletePersonajeBtnClick(object sender, EventArgs e) {
			if(this.cantPersonajes > 1){
				foreach(Control c in this.personajesPanel.Controls){
					if(c.Name == "personajeGB" + this.cantPersonajes){
						// Se elimina el elemento del panel
						GroupBox cb = c as GroupBox;
						this.personajesPanel.Controls.Remove(cb);
						// Se elimina la referencia del diccionario
						this.txtDiccionarioPFin.Remove(this.cantPersonajes);
						this.txtDiccionarioPInicio.Remove(this.cantPersonajes);
						this.pbDiccionarioPTexturas.Remove(this.cantPersonajes);
						this.cbDiccionarioPersonaje.Remove(this.cantPersonajes);
						this.personajePesoTerrenos.Remove(this.cantPersonajes);
						this.personajePictureBox.Remove(this.cantPersonajes);
						this.cantPersonajes--;
					}
				}	
			}
		}
		// Se crea evento que especifica que a que personaje se le modificaran los costos de viaje 
		private void ModificarCostos(Object sender, EventArgs e){
			Button btn = (Button)sender;
			string id = btn.Name.Replace("personajeConfBtn","");
			crearFormCostos(Int32.Parse(id));
		}
		// Función que crea el formulario de costos para cada personaje
		private void crearFormCostos(int id){
			this.formAuxiliar = new Form();
			formAuxiliar.StartPosition = FormStartPosition.CenterScreen;
			formAuxiliar.Name = "FormTexturas";
			formAuxiliar.Text = "Personaje #" + id;
			formAuxiliar.FormBorderStyle = FormBorderStyle.FixedDialog;
			formAuxiliar.MaximizeBox = false;
			formAuxiliar.MinimizeBox = false;
			formAuxiliar.AutoScroll = true;
			string path = Directory.GetCurrentDirectory();
			formAuxiliar.Icon = new Icon(path + "\\icons\\triforce.ico");
			// ToolTip para los elementos de texturas
			ToolTip formToolTip = new ToolTip(); 
	        formToolTip.Active = true; 
	        formToolTip.AutoPopDelay = 4000; 
	        formToolTip.InitialDelay = 600; 
	        formToolTip.ToolTipIcon = ToolTipIcon.Info;
			// Se crean los Picture box
			PictureBox picPer = new PictureBox();
			picPer.SetBounds(10, 10 , 60,60);
		    picPer.Name = "personaje" + id;
		    picPer.BorderStyle = BorderStyle.FixedSingle;
			picPer.SizeMode = PictureBoxSizeMode.StretchImage;
			picPer.Image = this.pbDiccionarioPTexturas[id].Image;
			picPer.BackColor = Color.FromArgb(26, 26, 26);
			picPer.Parent = formAuxiliar;
			formToolTip.SetToolTip(picPer, "Imagen del personaje");
			// Crea Label
			Label l = new Label();
			l.SetBounds(80, 20, 0, 0);
			l.Text = this.cbDiccionarioPersonaje[id].Text;
			l.Font = new Font("Arial", 20, FontStyle.Bold);
			l.Size = new Size (l.PreferredWidth, l.PreferredHeight);
			l.Parent = formAuxiliar;
			// Crea un Button para guardar la información
			Button btn = new Button();
        	btn.Name = "personajeCostoId" + id;
        	btn.SetBounds(230,40,40,40);
        	path = Directory.GetCurrentDirectory() + "\\icons\\save.png";
        	btn.BackgroundImage = Image.FromFile(path);
        	btn.BackgroundImageLayout = ImageLayout.Stretch;
        	btn.Click += new EventHandler(GuardarCostos);
        	btn.Parent = formAuxiliar;
        	formToolTip.SetToolTip(btn, "Guardar configuración de " +
        	                       "\nlos costos de movimiento.");
        	
			// Se crea la instancia GroupBox
			GroupBox gp = new GroupBox();
			gp.Name = "gpTexturas" + id;
 			gp.Text ="Terrenos y costos";
		    gp.Parent = formAuxiliar;
			int i = 0;
			Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[id];
			foreach(KeyValuePair<int, float> entry in pePersonajes){
				// Se crean los Picture box
				PictureBox picBox = new PictureBox();
				picBox.SetBounds(10, 20 + (80 * i), 60,60);
			    picBox.BorderStyle = BorderStyle.FixedSingle;
				picBox.SizeMode = PictureBoxSizeMode.StretchImage;
				picBox.Image = this.pbDiccionarioTexturas[entry.Key].Image;
				picBox.BackColor = this.pbDiccionarioTexturas[entry.Key].BackColor;
				picBox.Parent = gp;
				formToolTip.SetToolTip(picBox, "Vista previa de la textura.");
				// Se crea el Label con la información de las texturas
				Label lbl = new Label();
				lbl.SetBounds(80, 20 + (80 * i), 0, 0);
				lbl.Text = this.diccionarioIdsTexturas[entry.Key];
				lbl.Font = new Font("Arial", 16, FontStyle.Italic);
				lbl.Size = new Size (lbl.PreferredWidth, lbl.PreferredHeight);
				lbl.Parent = gp;
				// Se crea el TextBox con el valor correspondiente de peso para el terrno
				TextBox txt = new TextBox();
				txt.SetBounds(80, 50 + (80*i), 150, 20);
				txt.Name = "texturaTBId" + entry.Key;
				if(entry.Value == -1){
					txt.Text = "N/A";	
				}
				else{
					txt.Text = entry.Value.ToString();	
				}
			    txt.TextChanged += new EventHandler(TextRegexFloat);
				txt.Parent = gp;
				formToolTip.SetToolTip(txt, "Costo del terreno.");
				i++;
			}
			gp.SetBounds(10, 80, 260, (80 * i)  + 10);
			formAuxiliar.Height = 200 + (gp.Height/2);
			formAuxiliar.ShowDialog();
		}
		// Evento para el REGEX de Flotantes
		private void TextRegexFloat(Object sender, EventArgs e){
			TextBox txtbx = ((TextBox)sender);
			// Se verifica que el REGEX para flotantes y N/A.
			if (new Regex(@"^[0-9]*(?:\.[0-9]*)?$").IsMatch(txtbx.Text) || txtbx.Text == "N/A"){
				txtbx.BackColor = Color.Empty;
			}
			else {
				txtbx.BackColor = Color.LightBlue;
				ToolTip tt = new ToolTip();
    			tt.Show("Dato Entero, Punto Flotante o N/A.", txtbx, 2000);
			}
		}
		// Función que guarda los costos de viaje para cada terreno
		private void GuardarCostos(Object sender, EventArgs e){
			string pId;
			foreach(Control c in this.formAuxiliar.Controls){
				if(c.Name.Contains("gpTexturas")){
					pId = c.Name.Replace("gpTexturas","");
					foreach(Control con in c.Controls){
						if(con.Name.Contains("texturaTBId")){
							TextBox txtbx = con as TextBox;
							if (new Regex(@"^[0-9]*(?:\.[0-9]*)?$").IsMatch(txtbx.Text) || txtbx.Text == "N/A"){
								string tId = txtbx.Name.Replace("texturaTBId", "");
								Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[Int32.Parse(pId)];
								if(txtbx.Text == "N/A"){
									// En caso de N/A se inserta un  - 1
									pePersonajes[Int32.Parse(tId)] = -1;
								}
								else{
									// Se trunca el valor a 2 decimales
									if (txtbx.Text == ""){
										pePersonajes[Int32.Parse(tId)] = 0;
									}
									else{
										pePersonajes[Int32.Parse(tId)] = (float)(Math.Truncate((double)float.Parse(txtbx.Text)*100.0) / 100.0);
									}
								}
								
							}
							else {
								txtbx.Focus();
								return;
							}
						}
					}
				}
			}
			this.formAuxiliar.Dispose();
		}
		// Función para dibujar la primer cuadricula
		private void dibujarCuadricula(){
			// Carga el recuadro del mapa si fue leído correctamente el archivo
			Bitmap mapaBM = new Bitmap(this.mapaPbox.Width, this.mapaPbox.Height);
			Graphics mapaG = Graphics.FromImage(mapaBM);
			// Carga el recuadro para las Columnas.
			Bitmap cBM = new Bitmap(this.columnasPBox.Width, this.columnasPBox.Height);
			Graphics cG = Graphics.FromImage(cBM);
			// Carga el recuadro para las Filas.
			Bitmap fBM = new Bitmap(this.filasPBox.Width, this.filasPBox.Height);
			Graphics fG = Graphics.FromImage(fBM);
			// Variables de ancho y largo
			this.w = Convert.ToInt32((this.mapaPbox.Width / this.c) - 0.6);
			this.h = Convert.ToInt32((this.mapaPbox.Height / this.f) - 0.6);
			// Se definen los rectangulos de la cuadricula del mapa
			Rectangle rectanguloMapa = new Rectangle(0,0,w,h);
			// Se definen los rectangulos para las columnas
			Rectangle rectanguloColumnas = new Rectangle(0,0,Convert.ToInt32((this.columnasPBox.Width/this.c) - 0.6), 
			                                             Convert.ToInt32(this.columnasPBox.Height - 0.6));
			//Se definen los rectangulos para las filas
			Rectangle rectanguloFilas = new Rectangle(0,0, Convert.ToInt32(this.filasPBox.Width - 0.6),
			                                               Convert.ToInt32((this.filasPBox.Height/this.f)-0.6));
			int contador = 1;
			// Se dibuja la cuadricula del mapa
			for(int i = 0; i < this.f; i++){
				for(int j = 0; j < this.c; j++){
					// Dibuja rectanuglo en el mapa
					mapaG.DrawRectangle(Pens.Black, rectanguloMapa);
					rectanguloMapa.X += w;
					// Rellena el Diccionario de casillas
					this.diccionarioVisitas.Add((contador * 100) + (j + 1), "");
				}
				contador++;
				rectanguloMapa.Y += h ; 
				rectanguloMapa.X = 0;
			}
			// Dibuja los retangulos para las filas
			for(int i = 0; i < this.f; i++){
				fG.DrawRectangle(Pens.Black, rectanguloFilas);
				fG.DrawString((i+1).ToString(), new Font("Arial", 8), Brushes.Black, rectanguloFilas);
				rectanguloFilas.Y += Convert.ToInt32((this.filasPBox.Height/this.f) - 0.6);
			}
			// Dibuja los retangulos para las columnas
			for(int j = 0; j < this.c; j++){
				cG.DrawRectangle(Pens.Black, rectanguloColumnas);
				cG.DrawString(char.ConvertFromUtf32(j+65).ToString(), new Font("Arial", 8), Brushes.Black, rectanguloColumnas);
				rectanguloColumnas.X += Convert.ToInt32((this.columnasPBox.Width/this.c) - 0.6);
			}
			// Actualiza el PictureBox
			this.mapaPbox.Image = mapaBM;
			this.columnasPBox.Image = cBM;
			this.columnasPBox2.Image = cBM;
			this.filasPBox.Image = fBM;
			this.filasPBox2.Image = fBM;
		}
		// Función que integra las texturas dentro del mapa
		private void insertarTexturas(){
			// Carga el recuadro del mapa si fue leído correctamente el archivo
			Bitmap mapaBM = new Bitmap(this.mapaPbox.Width,this.mapaPbox.Height);
			this.mapaPbox.Image = (Image)mapaBM;
			// Crea una instancia para manejar los gráficos del Bitmap
			Graphics mapaG = Graphics.FromImage(mapaBM);
			// Variables de ancho y largo
			int w = Convert.ToInt32((this.mapaPbox.Width / this.c) - 0.6);
			int h = Convert.ToInt32((this.mapaPbox.Height / this.f) - 0.6);
			//Aqui se definen los rectangulos de la cuadricula
			Rectangle rectanguloMapa = new Rectangle(0,0,w,h);
			// Se dibuja la cuadricula del mapa
			for(int i = 0; i < this.f; i++){
				for(int j = 0; j < this.c; j++){
					// Dibuja rectanuglo en el mapa con la textura deseada
					if(this.pbDiccionarioTexturas[this.mapaMatriz[i,j]].Image != null){
						mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[i,j]].Image, rectanguloMapa);	
					}
					else{
						mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[i,j]].BackColor), rectanguloMapa);
					}
					// Se le da contorno al recuadro y continúa
					mapaG.DrawRectangle(Pens.Black, rectanguloMapa);
					rectanguloMapa.X += w;
				}
				rectanguloMapa.Y += h ; 
				rectanguloMapa.X = 0;
			}
			this.mapaPbox.Image = mapaBM;	
		}
		// Evento para abrir un mapa
		void AbrirMapaClick(object sender, EventArgs e) {
			// Se inicializa el valor por defecto en OK para el DialogResult
			DialogResult dResultado = new DialogResult();
			dResultado = DialogResult.OK;
			// En caso de existir un mapa en memoria, pregunta para sustituirlo
			if(this.diccionarioIdsTexturas.Count != 0){
				dResultado = MessageBox.Show("¿Desea sobrescribir los datos del mapa?", "Cargar nuevo mapa",
				                             MessageBoxButtons.OKCancel,MessageBoxIcon.Question);
			}
			// En caso de ser la primer lectura de archivo o de ser sustituido
			// se abre el buscador del archivo.
			if(dResultado == DialogResult.OK){
				if(leerArchivo()){
					// Se crea la cuadricula del mapa
					dibujarCuadricula();
					// Se rellena el panel de texturas
					rellenarTabTexturas();
					// Activa las opciones del Tab si se leyó correctamente un archivo
					this.opcionesTabControl.Enabled = true;
					this.buscarCasillaGP.Enabled = true;
					this.busquedaTxtBox.Text = "Ejemplo: A,1";
					this.busquedaTxtBox.ForeColor = Color.FromArgb(89, 89, 89);
					this.busquedaTxtBox.Font = new Font(this.busquedaTxtBox.Font, FontStyle.Italic);
					// ToolTip para indicarle al usuario que hacer
					ToolTip indicaciones = new ToolTip();
					indicaciones.IsBalloon = true;
					indicaciones.AutoPopDelay = 4000; 
					indicaciones.InitialDelay = 600; 
					indicaciones.IsBalloon = true; 
					indicaciones.ToolTipIcon = ToolTipIcon.Info;
					indicaciones.Show("Ahora puede editar los valores de los terrenos." +
					                  "\nUna vez terminado, deberá darle click al botón de guardado" +
					                  "\no en su defecto solo cambiar de Tab.", this.guardarTexturasBtn, 0, -90, 10000);
				}	
			}
		}
		// Terminar el programa
		void MainFormLoad(object sender, EventArgs e) {
			
		}
		// Boton de guardado de las configuraciones de las texturas
		void GuardarBtnClick(object sender, EventArgs e)
		{
			if(datosCompletosTexturas()){
				if(this.cambiosTexturas == true){
					insertarTexturas();	
					this.cambiosTexturas = false;
				}
				this.opcionesTabControl.SelectTab(1); 
				// ToolTip para indicarle al usuario que hacer
				ToolTip indicaciones = new ToolTip();
				indicaciones.IsBalloon = true;
				indicaciones.AutoPopDelay = 4000; 
				indicaciones.InitialDelay = 400; 
				indicaciones.IsBalloon = true; 
				indicaciones.Active = true;
				indicaciones.ToolTipIcon = ToolTipIcon.Info;
				indicaciones.Show("Ahora puede editar la posición Inicial/Final para el personaje." +
				                  "\nUna vez terminado, deberá darle click al botón de guardado.", this.guardarPersonajesBtn, 0, -90, 10000);		
			}
		}
		
		// Evento para limpiar todos los espacios de memoria
		void LimpiartBtnClick(object sender, EventArgs e) {
			liberarMemoria();
			this.opcionesTabControl.Enabled = false;
			this.mapaPbox.Focus();
			this.buscarCasillaGP.Enabled = false;
			this.busquedaTxtBox.Text = "Ejemplo: A,1";
			this.busquedaTxtBox.ForeColor = Color.FromArgb(89, 89, 89);
			this.busquedaTxtBox.Font = new Font(this.busquedaTxtBox.Font, FontStyle.Italic);
			this.playBtn.Enabled = false;
		}
		
		void GuardarPersonajesBtnClick(object sender, EventArgs e) {
			if(datosCompletosPersonajes()){
				this.playBtn.Enabled = true;
				this.playBtn.Focus();
				// ToolTip para indicarle al usuario que hacer
				ToolTip indicaciones = new ToolTip();
				indicaciones.IsBalloon = true;
				indicaciones.AutoPopDelay = 4000; 
				indicaciones.InitialDelay = 600; 
				indicaciones.IsBalloon = true; 
				indicaciones.ToolTipIcon = ToolTipIcon.Info;
				indicaciones.Show("Ahora con todos los datos guardados, " +
               "\npuede empezar a jugar :D", this.playBtn, 0, -90, 10000);
			}
		}
		
		void PlayBtnClick(object sender, EventArgs e) {
			if(this.diccionarioIdsTexturas.Count == 0){
				MessageBox.Show("No se puede iniciar el juego sin un mapa. " +
				                "\n¡Cargue uno para jugar! :D", "¡Sin mapa!",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			if(!datosCompletosTexturas()){
				MessageBox.Show("Datos incompletos en terrenos, llene todos los campos para empezar el juego.", "¡Datos faltantes en terrenos!",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			if(this.txtDiccionarioPFin.Count == 0){
				MessageBox.Show("Datos incompletos en personajes, llene todos los campos para empezar el juego.", "¡Datos faltantes en personajes!",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			if(!datosCompletosPersonajes()){
				MessageBox.Show("Datos incompletos en personajes o coordenadas incorrectas, " +
				                "verifique los datos de los campos para empezar el juego.", "¡Datos faltantes en personajes!",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			// Crear interfáz para elegir personaje con el cual se jugará
			formEleccionPersonaje();
			if(this.personajeJugable == 0){
				MessageBox.Show("No se puede iniciar el juego sin un personaje. " +
				                "\n¡Elija un personaje! :D", "¡Sin personaje! D:",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				return;
			}
			// Se desactiva el panel de modificación
			this.opcionesTabControl.Enabled = false;
			this.controlArchivoGB.Enabled = false;
			this.buscarCasillaGP.Enabled = false;
			// Se limpia el diccionario de visitas
			limpiarDiccionarioVisitas();
			this.cVisitas = 0;
			// Se separan las coordenadas de inicio del personaje
			string [] coordenadas = this.txtDiccionarioPInicio[this.personajeJugable].Text.Split(',');
			this.x_y_Inicio[0] = ((int)char.Parse(coordenadas[0]) - 65);
			this.x_y_Inicio[1] = (Int32.Parse(coordenadas[1]) - 1);
			// Se colocan las coordenadas de inicio en el diccionario
			this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 1) *100)] = "I:";
			// Se carga el nuevo lugar para el personaje
			this.personajePictureBox[this.personajeJugable].Location = 
				new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
			// Se leen las coordenadas finales para el personaje
			coordenadas = this.txtDiccionarioPFin[this.personajeJugable].Text.Split(',');
			this.x_y_Final[0] = ((int)char.Parse(coordenadas[0]) - 65);
			this.x_y_Final[1] = (Int32.Parse(coordenadas[1]) - 1);
			this.diccionarioVisitas[(this.x_y_Final[0] + 1) + ((this.x_y_Final[1] + 1) *100)] = "F:";
			// Crea la nieba al rededor del personaje
			Niebla();
			// Se limpia el mapa de resultados previos
			insertarTexturas();
			// Se limpia el árbol de nodos
			treeBusqueda.Nodes.Clear();
			switch(this.tipoBusquedaCB.SelectedIndex){
				case 0:
					this.cVisitas++;
					descubrirTerreno();
					this.KeyDown += KeyListener;
					DetenerJuego();
					break;
				case 1:
					// Evalúa por busqueda en profundidad
					BusquedaProfundidad();
					break;
				case 2:
					BusquedaCosteUniforme();
					break;
				case 3:
					BusquedaPrimeroVoraz();
					break;
				case 4: 
					BusquedaA();
					break;
				default:
					break;
			}
			
		}
		
		private void Niebla(){
			// Se elige la textura del personaje en cuestion
			string path = Directory.GetCurrentDirectory() + "\\pixel_characters\\";
			string pMusic = Directory.GetCurrentDirectory() + "\\sounds\\";
			ComboBox cb = this.cbDiccionarioPersonaje[this.personajeJugable];
			switch(cb.SelectedIndex){
				case 0:
					path += "green.png";
					pMusic += "green.wav";
					break;
				case 1:
					path += "red.png";
					pMusic += "red.wav";
					break;
				case 2:
					path += "purple.png";
					pMusic += "purple.wav";
					break;
				case 3:
					path += "blue.png";
					pMusic += "blue.wav";
					break;
				case 4:
					path += "black.png";
					pMusic += "black.wav";
					break;
				default:
					break;
			}
			// Se crea el Fog para el mapa
			this.pbFog = new PictureBox();
			this.pbFog.SetBounds(0, 0, this.mapaPbox.Width, this.mapaPbox.Height);
			this.pbFog.BackColor = Color.Black;
			this.pbFog.Parent = this.mapaPbox;
			this.pbFog.Image = new Bitmap(this.mapaPbox.Width,this.mapaPbox.Height);
			// Se pinta el personaje elegido y se coloca al frente del mapa
			this.personajePictureBox[this.personajeJugable].Image = new Bitmap(Image.FromFile(path));
			this.personajePictureBox[this.personajeJugable].Show();
			this.personajePictureBox[this.personajeJugable].BringToFront();
			// Carga la música del personaje
			this.musicaFondo.Stop();
			this.musicaFondo.Dispose();
			this.musicaFondo = new SoundPlayer(pMusic);
			this.musicaFondo.PlayLooping();
			// Descubre el terreno de la posición final
			Bitmap bm = new Bitmap(this.pbFog.Image, this.pbFog.Width, this.pbFog.Height);
			// Crea una instancia para manejar los gráfico
			Graphics mapaG = Graphics.FromImage(bm);
			// Se crea el rectángulo con  la textura a mostrar
			Rectangle r = new Rectangle(this.x_y_Final[0] * this.w,  this.x_y_Final[1] * this.h, this.w, this.h);
			// Dibuja el rectángulo actual del personaje
			if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Final[1], this.x_y_Final[0]]].Image != null) {
				mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Final[1], this.x_y_Final[0]]].Image, r);	
			}
			else {
				mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Final[1], this.x_y_Final[0]]].BackColor), r);
			}
			mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Final[0] + 1) + ((this.x_y_Final[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r.X, r.Y);
			mapaG.DrawRectangle(Pens.Black, r);
			this.pbFog.Image = bm;
		}
		
		// Mueve el bitmap sel personaje
		private void MoverPersonaje(){
			this.personajePictureBox[this.personajeJugable].Location =  new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
			descubrirTerreno();
		}
		
		// Estructura nodos para usarse en todas las búsquedas
		public class Nodo{
			public int Visita;
			public double Costo;
			public double HN;
			public double GN;
			public int Fila;
			public int Columna;
			public string Padre;
		}
		
		// Verifica si existe dentro de la pila
		private bool ExistePila(Stack<Nodo> pilaNodos, int sumaC, int sumaF){
			foreach (var element in pilaNodos) {
				if(element.Columna == (this.x_y_Inicio[0] + sumaC) && element.Fila == (this.x_y_Inicio[1] + sumaF)){
					return true;
				}
			}
			return false;
		}
		
		// Verifica si existe dentro de la lista de visitados
		private bool ExisteLista(List<Nodo> listaVisitados, int sumaC, int sumaF){
			foreach (var element in listaVisitados) {
				if(element.Columna == (this.x_y_Inicio[0] + sumaC) && element.Fila == (this.x_y_Inicio[1] + sumaF)){
					return true;
				}
			}
			return false;
		}
		
		// Método de búsqueda en profundidad
		private void BusquedaProfundidad(){
			Stack<Nodo> pilaNodos = new Stack<MainForm.Nodo>();
			List<Nodo> listaVisitados = new List<MainForm.Nodo>();
			// Se ingresa el nodo inicial
			pilaNodos.Push(new MainForm.Nodo{
				Visita = this.cVisitas,
				Costo = 0,
				Fila = this.x_y_Inicio[1],
				Columna = this.x_y_Inicio[0],
				Padre = "Ninguno",
			});
			
			// Empieza la evaluación de la búsqueda por profundidad
			while(!DetenerJuego()){
				//Pausa por un segundo esperando a mostrar en el mapa la actualización de Bitmaps
				Application.DoEvents();
				Thread.Sleep(1000);
				if(pilaNodos.Count > 0){
					// Se coloca el personaje en la siguiente posición y se descubre el mapa
					Nodo n  = pilaNodos.Peek();
					n.Visita = ++this.cVisitas;
					this.x_y_Inicio[0] = n.Columna;
					this.x_y_Inicio[1] = n.Fila;
					// Se desencola el nodo expandido y se agrega a la lista de visitados
					listaVisitados.Add(n);
					//Se inserta nodo para arbol
					if(n.Padre == "Ninguno"){
						treeBusqueda.Nodes.Clear();
						insertarPadre(n);
					}
					else {
						//Se busca el nodo hijo en el arbol y se actualiza la etiqueta, junto con coste y visita
						buscarHijo(n);
					}
					pilaNodos.Pop();
				}
				else{
					break;
				}
				if(DetenerJuego()){
					break;
				}
				Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[this.personajeJugable];
				// Se expanden los Nodos por orden de expansión
				for (int i = 0; i < this.ordenMovimiento.Count; i++) {
					// Se recorren los valores del orden de movimiento para saber su orden
					// En caso de poderse mover sobre el terreno, se agregan a la cola
					for (int j = 0; j < this.ordenMovimiento.Count; j++) {
						if(this.ordenMovimiento[j] == i){
							switch (j){
							//Arriba
							case 0:
			                    if(this.x_y_Inicio[1] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] != - 1) {
								if(!ExistePila(pilaNodos, 0, -1) && !ExisteLista(listaVisitados, 0, -1)){
										pilaNodos.Push(new Nodo{
											Visita = 0,
											Costo = 0,
											Fila = this.x_y_Inicio[1] - 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
										});
										insertarHijo(pilaNodos.Peek());
									}
			                    }
			                    break;
		                    //Derecha
			                case 1:
		                    	if(this.x_y_Inicio[0] + 1 < this.c  && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] > - 1) {
		                    		if(!ExistePila(pilaNodos, 1, 0) && !ExisteLista(listaVisitados, 1, 0)){
			                    		pilaNodos.Push(new Nodo{
											Visita = 0,
											Costo = 0,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] + 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
										});
										insertarHijo(pilaNodos.Peek());
		                    		}
			                    }
			                    break;
		                    //Abajo
			                case 2:
		                    	if(this.x_y_Inicio[1] + 1 < this.f && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] > - 1) {
									if(!ExistePila(pilaNodos, 0, 1) && !ExisteLista(listaVisitados, 0, 1)){
		                    			pilaNodos.Push(new Nodo{
											Visita = 0,
											Costo = 0,
											Fila = this.x_y_Inicio[1] + 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
										});
										insertarHijo(pilaNodos.Peek());
		                    		}
			                    }
			                    break;
		                    //Izquierda
			                case 3:
			                    if(this.x_y_Inicio[0] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] > - 1 ){
		                    		if(!ExistePila(pilaNodos, -1, 0) && !ExisteLista(listaVisitados, -1, 0)){
			                    		pilaNodos.Push(new Nodo{
											Visita = 0,
											Costo = 0,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] - 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
										});	
										insertarHijo(pilaNodos.Peek());
		                    		}
								}
			                    break;
				            }
						}
					}
				}
				MoverPersonaje();
			}
			CaminoRespuesta(ref listaVisitados);
		}
		
		// Colorea el camino respuesta dentro del mapa
		private void CaminoRespuesta(ref List<Nodo> listaVisitados){
			// Impresión del camino en términal
			Nodo n_2 = new Nodo();
			for (int i = listaVisitados.Count-1; i >= 0; i--) {
				// Toma el último elemento
				if(i == listaVisitados.Count-1){
					n_2 = listaVisitados[i];
					colorearCamino(listaVisitados[i].Columna, listaVisitados[i].Fila);
				}
				// Toma el primer elemento
				if(i == 0){
					n_2 = listaVisitados[i];
					colorearCamino(listaVisitados[i].Columna, listaVisitados[i].Fila);
				}
				// Compara el padre con los anteriores
				string padre = (char)(listaVisitados[i].Columna + 65) + (listaVisitados[i].Fila + 1).ToString();
				if(padre == n_2.Padre){
					n_2 = listaVisitados[i];
					colorearCamino(listaVisitados[i].Columna, listaVisitados[i].Fila);
				}
			}
			
		}
		
		
		private void colorearCamino(int x, int y){
			//Aqui se definen los rectangulos de la cuadricula
			int w = Convert.ToInt32((this.mapaPbox.Width / this.c) - 0.6);
			int h = Convert.ToInt32((this.mapaPbox.Height / this.f) - 0.6);
			int coordX = w * x;
			int	coordY = h * y;
			// Carga el recuadro del mapa si fue leído correctamente el archivo
			Bitmap mapaBM = (Bitmap)this.mapaPbox.Image;
			Graphics g = Graphics.FromImage(mapaBM);	
			// Se definen los rectangulos de la cuadricula del mapa
			Rectangle marcoCamino = new Rectangle(coordX, coordY, w, h);
			Pen pluma = new Pen(Color.Red,4);
			g.DrawRectangle(pluma, marcoCamino);
			this.mapaPbox.Image = mapaBM;
		}
		
		//insertar nodo padre
		private void insertarPadre(Nodo n){
			//Agregar etiqueta al arbol
			String myStr;
			int suma;
			char coord;
			
			//Coordenada x
			suma = n.Columna + 65;
			coord = Convert.ToChar(suma);
			myStr = coord.ToString();
			//Coordenada y
			suma = n.Fila + 1;
			myStr += suma.ToString();
			
			myStr += ": Visita = ";
			myStr += n.Visita.ToString();
			
			if(tipoBusquedaCB.SelectedIndex == 4){
				myStr += ": Coste -> ";
				myStr += n.GN.ToString("0.00");
				myStr += " + ";
				myStr += n.HN.ToString("0.00");
				myStr += " = ";
				myStr += n.Costo.ToString("0.00");
			}
			else if(tipoBusquedaCB.SelectedIndex != 1){
				myStr += ": Coste = ";
				myStr += n.Costo.ToString("0.00");
			}
			
			treeBusqueda.Nodes.Add(myStr);
		}
		
		//Crear treeview dinamico
		private void insertarHijo(Nodo n){
		//Agregar etiqueta al arbol
			string myStr;
			int suma;
			char coord;
			string nodoPadre;
			
			//Coordenada x
			suma = n.Columna + 65;
			coord = Convert.ToChar(suma);
			myStr = coord.ToString();
			//Coordenada y
			suma = n.Fila + 1;
			myStr += suma.ToString();
			
			myStr += ": Visita = ";
			myStr += n.Visita.ToString();
			
			if(n.Visita != 0){
				if(tipoBusquedaCB.SelectedIndex == 4){
					myStr += ": Coste -> ";
					myStr += n.GN.ToString("0.00");
					myStr += " + ";
					myStr += n.HN.ToString("0.00");
					myStr += " = ";
					myStr += n.Costo.ToString("0.00");
				}
				else if(tipoBusquedaCB.SelectedIndex != 1){
					myStr += ": Coste = ";
					myStr += n.Costo.ToString("0.00");
				}
			}
			
			//Se busca al padre de ese nodo, para insertarlo como hijo
			nodoPadre = n.Padre;
			TreeNode padre = SearchTreeView(nodoPadre, treeBusqueda.Nodes);
			if(padre != null)
				padre.Nodes.Add(myStr);
		}
		
		private void buscarHijo(Nodo n){
			string myStr;
			int suma;
			char coord;
			
			//Coordenada x
			suma = n.Columna + 65;
			coord = Convert.ToChar(suma);
			myStr = coord.ToString();
			//Coordenada y
			suma = n.Fila + 1;
			myStr += suma.ToString();
			
			TreeNode hijo = SearchTreeView(myStr, treeBusqueda.Nodes);
			if(hijo != null){
				myStr += ": Visita = ";
				myStr += n.Visita.ToString();
				
				if(n.Visita != 0){
					if(tipoBusquedaCB.SelectedIndex == 4){
						myStr += ": Coste -> ";
						myStr += n.GN.ToString("0.00");
						myStr += " + ";
						myStr += n.HN.ToString("0.00");
						myStr += " = ";
						myStr += n.Costo.ToString("0.00");
					}
					else if(tipoBusquedaCB.SelectedIndex != 1){
						myStr += ": Coste = ";
						myStr += n.Costo.ToString("0.00");
					}
					
					hijo.Text = myStr;
				}
			}
		}
		
		TreeNode SearchTreeView (string p_sSearchTerm, TreeNodeCollection p_Nodes){
			int found = 0;
			string aux = "";
			string myStr = "";
			
			foreach (TreeNode node in p_Nodes){
				aux = node.Text;
				found = aux.IndexOf(":");
				myStr = aux.Substring(0,found);
				
				if (myStr == p_sSearchTerm)
					return node;

				if (node.Nodes.Count > 0) //si tiene hijos
				{
					TreeNode child = SearchTreeView(p_sSearchTerm, node.Nodes);
					if (child != null) return child;
				}
			}
			return null;
		}
		
		// Busqueda por coste uniforme
		private void BusquedaCosteUniforme(){
			List<Nodo> colaNodos = new List<Nodo>{};
			List<Nodo> listaVisitados = new List<Nodo>{};
			// Se ingresa el nodo inicial
			colaNodos.Add(new Nodo{
				Visita = this.cVisitas,
				Costo = 0,
				Fila = this.x_y_Inicio[1],
				Columna = this.x_y_Inicio[0],
				Padre = "Ninguno",
			});
			do {
				Application.DoEvents();
				Thread.Sleep(1000);
				Nodo n = new Nodo();
				if(colaNodos.Count > 0){
					// Se coloca el personaje en la siguiente posición y se descubre el mapa
					n  = colaNodos[0];
					n.Visita = ++this.cVisitas;
					this.x_y_Inicio[0] = n.Columna;
					this.x_y_Inicio[1] = n.Fila;
					// Se desencola el nodo expandido y se agrega a la lista de visitados
					listaVisitados.Add(n);
					//Se inserta nodo para arbol
					if(n.Padre == "Ninguno"){
						treeBusqueda.Nodes.Clear();
						insertarPadre(n);
					}
					else{
						//Se busca el nodo hijo en el arbol y se actualiza la etiqueta, junto con coste y visita
						buscarHijo(n);
					}
					colaNodos.RemoveAt(0);
				}
				else{
					break;
				}
				if(DetenerJuego()){
					break;
				}
				Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[this.personajeJugable];
				// Se expanden los Nodos por orden de expansión
				for (int i = 0; i < this.ordenMovimiento.Count; i++) {
					// Se recorren los valores del orden de movimiento para saber su orden
					// En caso de poderse mover sobre el terreno, se agregan a la cola
					for (int j = 0; j < this.ordenMovimiento.Count; j++) {
						if(this.ordenMovimiento[j] == i){
							switch (j){
							//Arriba
							case 0:
			                    if(this.x_y_Inicio[1] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] != - 1) {
									if(!ExisteLista(colaNodos, 0, -1) && !ExisteLista(listaVisitados, 0, -1)){
										InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.Costo,
											Fila = this.x_y_Inicio[1] - 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
						                });
										insertarHijo(nodoCola);
										
									} else {
										if(ExisteLista(colaNodos, 0, - 1) && !ExisteLista(listaVisitados, 0, -1)){
											EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.Costo,
												Fila = this.x_y_Inicio[1] - 1,
												Columna = this.x_y_Inicio[0],
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
						                	}, 0, -1);
										}
		                    		}
			                    }
			                    break;
		                    //Derecha
			                case 1:
		                    	if(this.x_y_Inicio[0] + 1 < this.c  && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] > - 1) {
			                    	if(!ExisteLista(colaNodos, 1, 0) && !ExisteLista(listaVisitados, 1, 0)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.Costo,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] + 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
										
		                    		} else {
			                    		if(ExisteLista(colaNodos, 1, 0) && !ExisteLista(listaVisitados, 1, 0)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.Costo,
												Fila = this.x_y_Inicio[1],
												Columna = this.x_y_Inicio[0] + 1,
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	            		                  	}, 1, 0);
			                    		}
		                    		}
		                    	}
			                    break;
		                    //Abajo
			                case 2:
		                    	if(this.x_y_Inicio[1] + 1 < this.f && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] > - 1) {
									if(!ExisteLista(colaNodos, 0, 1) && !ExisteLista(listaVisitados, 0, 1)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.Costo,
											Fila = this.x_y_Inicio[1] + 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
										
		                    		} else {
			                    		if(ExisteLista(colaNodos, 0, 1) && !ExisteLista(listaVisitados, 0, 1)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.Costo,
												Fila = this.x_y_Inicio[1] + 1,
												Columna = this.x_y_Inicio[0],
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	                		                }, 0, 1);
			                    		}
		                    		} 
			                    }
			                    break;
		                    //Izquierda
			                case 3:
			                    if(this.x_y_Inicio[0] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] > - 1 ){
		                    		if(!ExisteLista(colaNodos, -1, 0) && !ExisteLista(listaVisitados, -1, 0)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.Costo,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] - 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
										
		                    		} else {
			                    		if(ExisteLista(colaNodos, -1, 0) && !ExisteLista(listaVisitados, -1, 0)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.Costo,
												Fila = this.x_y_Inicio[1],
												Columna = this.x_y_Inicio[0] - 1,
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	                		                }, -1, 0);
			                    		}
		                    		}
								}
			                    break;
				            }
						}
					}
				}
				MoverPersonaje();
			} while(!DetenerJuego());
			CaminoRespuesta(ref listaVisitados);
		}

		// Inserta de forma ordenada a la "cola"
		private void InsertaOrdenado(ref List<Nodo> colaNodos, Nodo n){
			/*List<Nodo> aux = new List<Nodo>{};
			List<Nodo> aux_2 = new List<Nodo>{};
			foreach(Nodo element in colaNodos){
				if(element.Costo <= n.Costo){
					aux.Add(element);
				} else {
					aux_2.Add(element);
				}
			}
			aux.Add(n);
			foreach (Nodo element in aux_2) {
				aux.Add(element);
			}
			colaNodos.Clear();
			colaNodos = aux;*/
			int index = 0;
			for(int i = 0; i < colaNodos.Count; i++){
				if(n.Costo >= colaNodos[i].Costo){
					index = i + 1;
				}
			}
			if(index == colaNodos.Count){
				colaNodos.Add(n);
			} else {
				colaNodos.Insert(index, n);
			}
			nodoCola = n;
		}
		
		// Se eliminan los duplicados dentro de la cola, en caso de que ser mejor opción el nuevo nodo
		private void EliminarDuplicado(ref List<Nodo> colaNodos, Nodo n, int sumaC, int sumaF){
			Nodo aux = new Nodo();
			int i = 0;
			foreach (var element in colaNodos) {
				if(element.Columna == (this.x_y_Inicio[0] + sumaC) && element.Fila == (this.x_y_Inicio[1] + sumaF)){
					aux = element;
					break;
				}
				i++;
			}
			if(n.Costo < aux.Costo){
				colaNodos.RemoveAt(i);
				buscarHijo(n);
				InsertaOrdenado(ref colaNodos, n);
			}
		}
		
		private void BusquedaPrimeroVoraz(){
			Stack<Nodo> nodosAbiertos = new Stack<Nodo>{};
			List<Nodo> nodosCerrados = new List<Nodo>{};
			double h_n = 0;
			if(this.medidaOpc == 0){
				h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
			} else {
				h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
			}
			// Se ingresa el nodo inicial
			nodosAbiertos.Push(new Nodo{
				Visita = this.cVisitas,
				Costo = h_n,
				Fila = this.x_y_Inicio[1],
				Columna = this.x_y_Inicio[0],
				Padre = "Ninguno",
			});
			//Se añade el nodo Raiz al treeView
			nodoCola = nodosAbiertos.Peek();
			treeBusqueda.Nodes.Clear();
			insertarPadre(nodoCola);
			BusquedaPrimeroVoraz(ref nodosAbiertos, ref nodosCerrados);
			CaminoRespuesta(ref nodosCerrados);
		}
		
		private void BusquedaPrimeroVoraz(ref Stack<Nodo> nodosAbiertos, ref List<Nodo> nodosCerrados){
			Application.DoEvents();
			Thread.Sleep(1000);
			Nodo actual = new Nodo();
			double h_n = 0;
			Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[this.personajeJugable];
			if(nodosAbiertos.Count > 0){
				actual = nodosAbiertos.Peek();
				actual.Visita = ++this.cVisitas;
				//Se busca al hijo para intercambiarlo.
				buscarHijo(actual);
				this.x_y_Inicio[0] = actual.Columna;
				this.x_y_Inicio[1] = actual.Fila;
				nodosAbiertos.Pop();
				nodosCerrados.Add(actual);
			} else {
				return;
			}
			if(DetenerJuego()){
				return;
			}
			List<Nodo> colaNodos = new List<Nodo>{};
			// Se expanden los Nodos por orden de expansión
			for (int i = 0; i < this.ordenMovimiento.Count; i++) {
				// Se recorren los valores del orden de movimiento para saber su orden
				// En caso de poderse mover sobre el terreno, se agregan a la cola
				for (int j = 0; j < this.ordenMovimiento.Count; j++) {
					if(this.ordenMovimiento[j] == i){
						switch (j){
						//Arriba
						case 0:
		                    if(this.x_y_Inicio[1] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] != - 1) {
								if(!ExistePila(nodosAbiertos, 0, -1) && !ExisteLista(nodosCerrados, 0, -1)){
									if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs((this.x_y_Inicio[1]) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow((this.x_y_Inicio[1]) - (this.x_y_Final[1] + 1), 2));
									}
									InsertaOrdenado(ref colaNodos, new Nodo{
										Visita = 0,
										Costo = h_n,
										Fila = this.x_y_Inicio[1] - 1,
										Columna = this.x_y_Inicio[0],
										Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
					                });
									insertarHijo(nodoCola);
								}
							}
		                    break;
	                    //Derecha
		                case 1:
	                    	if(this.x_y_Inicio[0] + 1 < this.c  && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] > - 1) {
	                    		if(!ExistePila(nodosAbiertos, 1, 0) && !ExisteLista(nodosCerrados, 1, 0)){
									if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 2) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 2) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
									}
	                    			InsertaOrdenado(ref colaNodos, new Nodo{
										Visita = 0,
										Costo = h_n,
										Fila = this.x_y_Inicio[1],
										Columna = this.x_y_Inicio[0] + 1,
										Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
            		                });
									insertarHijo(nodoCola);
	                    		}
	                    	}
		                    break;
	                    //Abajo
		                case 2:
	                    	if(this.x_y_Inicio[1] + 1 < this.f && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] > - 1) {
	                    		if(!ExistePila(nodosAbiertos, 0, 1) && !ExisteLista(nodosCerrados, 0, 1)){
									if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 2) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 2) - (this.x_y_Final[1] + 1), 2));
									}
	                    			InsertaOrdenado(ref colaNodos, new Nodo{
										Visita = 0,
										Costo = h_n,
										Fila = this.x_y_Inicio[1] + 1,
										Columna = this.x_y_Inicio[0],
										Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
            		                });
									insertarHijo(nodoCola);
	                    		}
		                    }
		                    break;
	                    //Izquierda
		                case 3:
		                    if(this.x_y_Inicio[0] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] > - 1 ){
	                    		if(!ExistePila(nodosAbiertos, -1, 0) && !ExisteLista(nodosCerrados, -1, 0)){
									if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0]) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0]) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
									}
	                    			InsertaOrdenado(ref colaNodos, new Nodo{
										Visita = 0,
										Costo = h_n,
										Fila = this.x_y_Inicio[1],
										Columna = this.x_y_Inicio[0] - 1,
										Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
            		                });
									insertarHijo(nodoCola);
	                    		}
							}
		                    break;
			            }
					}
				}
			}
			// Se insertan los nodos ordenados por cada padre
			for (int i = colaNodos.Count - 1; i >= 0 ; i--) {
				nodosAbiertos.Push(colaNodos[i]);
			}
			MoverPersonaje();
			BusquedaPrimeroVoraz(ref nodosAbiertos, ref nodosCerrados);
		}
		
		
		// Busqueda por coste uniforme
		private void BusquedaA(){
			Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[this.personajeJugable];
			List<Nodo> colaNodos = new List<Nodo>{};
			List<Nodo> listaVisitados = new List<Nodo>{};
			// Se ingresa el nodo inicial
			double h_n = 0;
			if(this.medidaOpc == 0){
				h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
			} else {
				h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
			}
			colaNodos.Add(new Nodo{
				Visita = this.cVisitas,
				HN = h_n,
				GN = 0,
				Costo = h_n,
				Fila = this.x_y_Inicio[1],
				Columna = this.x_y_Inicio[0],
				Padre = "Ninguno",
			});

			do {
				Application.DoEvents();
				Thread.Sleep(1000);
				Nodo n = new Nodo();
				if(colaNodos.Count > 0){
					// Se coloca el personaje en la siguiente posición y se descubre el mapa
					n  = colaNodos[0];
					n.Visita = ++this.cVisitas;
					this.x_y_Inicio[0] = n.Columna;
					this.x_y_Inicio[1] = n.Fila;
					// Se desencola el nodo expandido y se agrega a la lista de visitados
					listaVisitados.Add(n);
					colaNodos.RemoveAt(0);
				}
				else{
					break;
				}
				if(DetenerJuego()){
					break;
				}
				// Se expanden los Nodos por orden de expansión
				for (int i = 0; i < this.ordenMovimiento.Count; i++) {
					// Se recorren los valores del orden de movimiento para saber su orden
					// En caso de poderse mover sobre el terreno, se agregan a la cola
					for (int j = 0; j < this.ordenMovimiento.Count; j++) {
						if(this.ordenMovimiento[j] == i){
							switch (j){
							//Arriba
							case 0:
			                    if(this.x_y_Inicio[1] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] != - 1) {
									if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs((this.x_y_Inicio[1]) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow((this.x_y_Inicio[1]) - (this.x_y_Final[1] + 1), 2));
									}
									if(!ExisteLista(colaNodos, 0, -1) && !ExisteLista(listaVisitados, 0, -1)){
										InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											HN = h_n,
											GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.GN,											
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.GN + h_n,
											Fila = this.x_y_Inicio[1] - 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
						                });
										insertarHijo(nodoCola);
									} else{
										if(ExisteLista(colaNodos, 0, - 1) && !ExisteLista(listaVisitados, 0, -1)){
											EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												HN = h_n,
												GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.GN,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] + n.GN + h_n,
												Fila = this.x_y_Inicio[1] - 1,
												Columna = this.x_y_Inicio[0],
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
						                	}, 0, -1);
										}							
									}
			                    }
			                    break;
		                    //Derecha
			                case 1:
		                    	if(this.x_y_Inicio[0] + 1 < this.c  && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] > - 1) {
		                    		if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 2) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 2) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
									}
			                    	if(!ExisteLista(colaNodos, 1, 0) && !ExisteLista(listaVisitados, 1, 0)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											HN = h_n,
											GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.GN,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.GN + h_n,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] + 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
			                    	} else {
			                    		if(ExisteLista(colaNodos, 1, 0) && !ExisteLista(listaVisitados, 1, 0)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												HN = h_n,
												GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.GN,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] + n.GN + h_n,
												Fila = this.x_y_Inicio[1],
												Columna = this.x_y_Inicio[0] + 1,
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	            		                  	}, 1, 0);
			                    		}
			                    	}
		                    	}
			                    break;
		                    //Abajo
			                case 2:
		                    	if(this.x_y_Inicio[1] + 1 < this.f && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] > - 1) {
		                    		if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 2) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0] + 1) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 2) - (this.x_y_Final[1] + 1), 2));
									}
									if(!ExisteLista(colaNodos, 0, 1) && !ExisteLista(listaVisitados, 0, 1)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											HN = h_n,
											GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.GN,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.GN + h_n,
											Fila = this.x_y_Inicio[1] + 1,
											Columna = this.x_y_Inicio[0],
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
			                    	} else {
			                    		if(ExisteLista(colaNodos, 0, 1) && !ExisteLista(listaVisitados, 0, 1)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												HN = h_n,
												GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.GN,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] + n.GN + h_n,
												Fila = this.x_y_Inicio[1] + 1,
												Columna = this.x_y_Inicio[0],
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	                		                }, 0, 1);
			                    		}
			                    	}
			                    }
			                    break;
		                    //Izquierda
			                case 3:
			                    if(this.x_y_Inicio[0] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] > - 1 ){
		                    		if(this.medidaOpc == 0){
										h_n = Math.Abs((this.x_y_Inicio[0]) - (this.x_y_Final[0] + 1)) + Math.Abs( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1));
									} else {
										h_n = Math.Sqrt(Math.Pow((this.x_y_Inicio[0]) - (this.x_y_Final[0] + 1), 2) + Math.Pow( (this.x_y_Inicio[1] + 1) - (this.x_y_Final[1] + 1), 2));
									}
		                    		if(!ExisteLista(colaNodos, -1, 0) && !ExisteLista(listaVisitados, -1, 0)){
		                    			InsertaOrdenado(ref colaNodos, new Nodo{
											Visita = 0,
											HN = h_n,
											GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.GN,
											Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.GN +h_n,
											Fila = this.x_y_Inicio[1],
											Columna = this.x_y_Inicio[0] - 1,
											Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
                		                });
										insertarHijo(nodoCola);
			                    	} else {
			                    		if(ExisteLista(colaNodos, -1, 0) && !ExisteLista(listaVisitados, -1, 0)){
			                    			EliminarDuplicado(ref colaNodos, new Nodo{
												Visita = 0,
												HN = h_n,
												GN = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.GN,
												Costo = pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] + n.GN +h_n,
												Fila = this.x_y_Inicio[1],
												Columna = this.x_y_Inicio[0] - 1,
												Padre = (char)(this.x_y_Inicio[0] + 65) + (this.x_y_Inicio[1]+1).ToString(),
	                		                }, -1, 0);
			                    		}
			                    	}
								}
			                    break;
				            }
						}
					}
				}
				MoverPersonaje();
			} while(!DetenerJuego());
			
			crearArbolEstrella(ref listaVisitados, ref colaNodos);
			CaminoRespuesta(ref listaVisitados);
		}
		
		private void crearArbolEstrella(ref List<Nodo> listaVisitados, ref List<Nodo> colaNodos){
			List<Nodo> nuevo = new List<Nodo>() {};
			Nodo n = new Nodo();
			//Para lista visitados
			for (int i = listaVisitados.Count-1; i >= 0; i--) {
				n = listaVisitados[i];
				nuevo.Add(n);
				
				if(n.Padre == "Ninguno"){
					treeBusqueda.Nodes.Clear();
					insertarPadre(n);
				}
			}
			
			for (int i = nuevo.Count-1; i >= 0; i--) {
				n = nuevo[i];
				
				//Se inserta nodo para arbol
				insertarHijo(n);
			}
			
			//Para cola de no visitados
			for (int i = colaNodos.Count-1; i >= 0; i--) {
				n = colaNodos[i];
				insertarHijo(n);
			}
		}
		
		// Se limpia el diccionario de visitas
		private void limpiarDiccionarioVisitas(){
			for(int i = 1; i <= this.diccionarioVisitas.Count; i++){
				this.diccionarioVisitas[i] = "";
			}
		}
		
		// Función que revela la posición actual y vecinas del personaje
		private void descubrirTerreno(){
			Bitmap bm = new Bitmap(this.pbFog.Image, this.pbFog.Width, this.pbFog.Height);
			Bitmap bmMapaReal = new Bitmap(this.mapaPbox.Image);
			// Crea una instancia para manejar los gráfico
			Graphics mapaG = Graphics.FromImage(bm);
			Graphics mapaRealG = Graphics.FromImage(bmMapaReal);
			// Se crea el rectángulo con  la textura a mostrar
			Rectangle r = new Rectangle(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h, this.w, this.h);
			// Dibuja el rectángulo actual del personaje
			if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0]]].Image != null) {
				mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0]]].Image, r);	
				mapaRealG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0]]].Image, r);	
			}
			else {
				mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0]]].BackColor), r);
				mapaRealG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0]]].BackColor), r);
			}
			
			// Aumenta el contador en la casilla actual y escribe el número de visita
			string corroboración = this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 1) *100)].Replace("I:", "");
			if(corroboración.Split(',').Length <= 4){
				this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 1) *100)] += this.cVisitas.ToString() + ",";
			}
			
			// Se le escribe a la casilla la visita
			mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r.X, r.Y);
			mapaG.DrawRectangle(Pens.Black, r);
			mapaRealG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r.X, r.Y);
			mapaRealG.DrawRectangle(Pens.Black, r);
			// Dibuja una fila por debajo
			if(this.x_y_Inicio[1] + 1 < this.f){
				// Se dibuja el recuadro inferior al personaje
				Rectangle r_2 = new Rectangle((this.x_y_Inicio[0] * this.w),  ((this.x_y_Inicio[1] + 1) *this.h), this.w, this.h);
				if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]].Image != null) {
					mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]].Image, r_2);	
					mapaRealG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]].Image, r_2);	
				}
				else{
					mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]].BackColor), r_2);
					mapaRealG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]].BackColor), r_2);
				}
				mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 2) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaG.DrawRectangle(Pens.Black, r_2);
				
				mapaRealG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1] + 2) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaRealG.DrawRectangle(Pens.Black, r_2);
				
			}
			// Dibuja una columna por Derecha
			if(this.x_y_Inicio[0] + 1 < this.c){
				// Se dibuja el recuadro a la derecha del personaje
				Rectangle r_2 = new Rectangle((this.x_y_Inicio[0] + 1) * this.w, (this.x_y_Inicio[1]*this.h), this.w, this.h);
				if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]].Image != null) {
					mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]].Image, r_2);	
					mapaRealG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]].Image, r_2);
				}
				else{
					mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]].BackColor), r_2);
					mapaRealG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]].BackColor), r_2);
				}
				mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 2) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaG.DrawRectangle(Pens.Black, r_2);
				mapaRealG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 2) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaRealG.DrawRectangle(Pens.Black, r_2);
				
			}
			
			// Dibuja una fila por Arriba
			if(this.x_y_Inicio[1] - 1 >= 0){
				// Se dibuja el recuadro inferior al personaje
				Rectangle r_2 = new Rectangle((this.x_y_Inicio[0] * this.w),  ((this.x_y_Inicio[1] - 1) *this.h), this.w, this.h);
				if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]].Image != null) {
					mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]].Image, r_2);	
					mapaRealG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]].Image, r_2);
				}
				else{
					mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]].BackColor), r_2);
					mapaRealG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]].BackColor), r_2);
				}
				mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1]) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaG.DrawRectangle(Pens.Black, r_2);
				mapaRealG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0] + 1) + ((this.x_y_Inicio[1]) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaRealG.DrawRectangle(Pens.Black, r_2);
				
			}
			// Dibuja una columna por Izquierda
			if(this.x_y_Inicio[0] - 1 >= 0){
				// Se dibuja el recuadro a la derecha del personaje
				Rectangle r_2 = new Rectangle((this.x_y_Inicio[0] - 1) * this.w,  (this.x_y_Inicio[1]*this.h), this.w, this.h);
				if(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]].Image != null) {
					mapaG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]].Image, r_2);	
					mapaRealG.DrawImage(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]].Image, r_2);	
				}
				else{
					mapaG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]].BackColor), r_2);
					mapaRealG.FillRectangle(new SolidBrush(this.pbDiccionarioTexturas[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]].BackColor), r_2);
				}
				mapaG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0]) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaG.DrawRectangle(Pens.Black, r_2);
				mapaRealG.DrawString(this.diccionarioVisitas[(this.x_y_Inicio[0]) + ((this.x_y_Inicio[1] + 1) *100)], 
				                 new Font("Arial", 2 + (this.w/10)), new SolidBrush(Color.White), r_2.X, r_2.Y);
				mapaRealG.DrawRectangle(Pens.Black, r_2);
				
			}
			this.pbFog.Image = bm;
			this.mapaPbox.Image = bmMapaReal;
		}
		// Crea el form que mostrará la elección del personaje
		private void formEleccionPersonaje(){
			this.formAuxiliar = new Form();
			formAuxiliar.StartPosition = FormStartPosition.CenterScreen;
			formAuxiliar.Name = "formEleccionPersonaje";
			formAuxiliar.Text = "¡Elije a tu personaje!";
			formAuxiliar.FormBorderStyle = FormBorderStyle.FixedDialog;
			formAuxiliar.MaximizeBox = false;
			formAuxiliar.MinimizeBox = false;
			formAuxiliar.AutoSize = true;
			string path = Directory.GetCurrentDirectory();
			formAuxiliar.Icon = new Icon(path + "\\icons\\triforce.ico");	
			// Se crea la instancia GroupBox
			GroupBox gp = new GroupBox();
			gp.Name = "gpPersonajes";
 			gp.Text ="Personajes disponibles";
		    gp.Parent = formAuxiliar;
			int i = 0;
			foreach(KeyValuePair<int, PictureBox> entry in this.pbDiccionarioPTexturas){
				// Se crean los Picture box con la imagen del personaje
				PictureBox picBox = new PictureBox();
				picBox.SetBounds(10, 20 + (80 * i), 60,60);
				picBox.Name = "pId" + entry.Key;
			    picBox.BorderStyle = BorderStyle.FixedSingle;
				picBox.SizeMode = PictureBoxSizeMode.StretchImage;
				picBox.Image = this.pbDiccionarioPTexturas[entry.Key].Image;
				picBox.BackColor = Color.FromArgb(26, 26, 26);
				picBox.Parent = gp;
				// Se crea el Label con el número de personaje
				Label lbl = new Label();
				lbl.SetBounds(80, 20 + (80 * i), 0, 0);
				lbl.Text = "#" + (i + 1) +  ": " + this.cbDiccionarioPersonaje[entry.Key].Text ;
				lbl.Font = new Font("Arial", 14, FontStyle.Italic);
				lbl.Size = new Size (lbl.PreferredWidth, lbl.PreferredHeight);
				lbl.Parent = gp;
				// Se crea el Button para la elección de personaje
				Button btn = new Button();
				btn.SetBounds(80, 50 + (80*i), 150, 35);
				btn.Name = "perBtnid" + entry.Key;
				btn.Text = "¡Eligeme!";
				btn.Click += new EventHandler(EleccionPersonaje);
				btn.Parent = gp;
				i++;
			}
			gp.SetBounds(10, 10, 260, (80 * i)  + 10);
			formAuxiliar.ShowDialog();
		}
		// Crea el evento que eliminará el Form y asignará el personaje con el que se jugará
		private void EleccionPersonaje(Object sender, EventArgs e){
			Button btn = (Button)sender;
			string id = btn.Name.Replace("perBtnid", "");
			this.personajeJugable = Int32.Parse(id);
			this.formAuxiliar.Dispose();
		}
		
		// Evento para cada una de las teclas.
		private void KeyListener(Object sender, KeyEventArgs e) {	
			// Instancia que recupera el valor en peso de la casilla en el mapa
			Dictionary<int,float> pePersonajes = this.personajePesoTerrenos[this.personajeJugable];
			switch (e.KeyCode){
                case Keys.Left:
					// Se cálcula el movimiento del PictureBox a la izquierda
					if(this.x_y_Inicio[0] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] - 1]] > -1 ){
						this.x_y_Inicio[0]--;
						this.personajePictureBox[this.personajeJugable].Location = new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
						// Descibre el terreno en la posición actual del personaje
						this.cVisitas++;
						descubrirTerreno();
					}
                    break;
                case Keys.Right:
                    // Se cálcula el movimiento del PictureBox a la Derecha
                    if(this.x_y_Inicio[0] + 1 < this.c  && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1], this.x_y_Inicio[0] + 1]] > -1) {
						this.x_y_Inicio[0]++;
						this.personajePictureBox[this.personajeJugable].Location = new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
						this.cVisitas++;
						// Descibre el terreno en la posición actual del personaje
						descubrirTerreno();
                    }
                    break;
                case Keys.Up:
                    // Se cálcula el movimiento del PictureBox arriba
                    if(this.x_y_Inicio[1] - 1 >= 0 && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] - 1, this.x_y_Inicio[0]]] != -1) {
						this.x_y_Inicio[1]--;
						this.personajePictureBox[this.personajeJugable].Location = new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
						// Descibre el terreno en la posición actual del personaje
						this.cVisitas++;
						descubrirTerreno();
                    }
                    break;
                case Keys.Down:
                    // Se cálcula el movimiento del PictureBox abajo
                    if(this.x_y_Inicio[1] + 1 < this.f && pePersonajes[this.mapaMatriz[this.x_y_Inicio[1] + 1, this.x_y_Inicio[0]]] > -1) {
						this.x_y_Inicio[1]++;
						this.personajePictureBox[this.personajeJugable].Location =  new Point(this.x_y_Inicio[0] * this.w,  this.x_y_Inicio[1] * this.h);
						// Descibre el terreno en la posición actual del personaje
						this.cVisitas++;
						descubrirTerreno();
                    }
                    break;
            }
			DetenerJuego();
		}
		
		// Detener el juego
		private bool DetenerJuego(){
			// Se evalúa si el personaje ya llegó a la meta
			if(this.x_y_Inicio[0] == this.x_y_Final[0] && this.x_y_Inicio[1] == this.x_y_Final[1]){
				if(this.tipoBusquedaCB.SelectedIndex != 0){
					MoverPersonaje();
				}
				// Se detiene la música y reproduce la de victoria
				this.musicaFondo.Stop();
				this.musicaFondo.Dispose();
				SoundPlayer ss = new SoundPlayer(Directory.GetCurrentDirectory() + "\\sounds\\itemcatch.wav");
				ss.Play();
				// Muestra mensaje para felicitar al jugador
				MessageBox.Show("¡Felicidades! Haz logrado que "+ this.cbDiccionarioPersonaje[this.personajeJugable].Text.ToString()
				                +" llegue a la meta. :D", "¡El juego ha terminado!",
								                MessageBoxButtons.OK,MessageBoxIcon.Information);
				// Libera memoria para reiniciar el juego
				this.personajePictureBox[this.personajeJugable].Image = null;
				// Se elimina el Listener del juego
				if(this.tipoBusquedaCB.SelectedIndex == 0){
					this.KeyDown -= KeyListener;
				}
				this.opcionesTabControl.Enabled = true;
				this.controlArchivoGB.Enabled = true;
				this.buscarCasillaGP.Enabled = true;
				this.pbFog.Dispose();
				this.personajePictureBox[this.personajeJugable].Hide();
				this.personajeJugable = 0;
				// Reinicia la música del juego nuevamente
				this.musicaFondo = new SoundPlayer(Directory.GetCurrentDirectory() + "\\sounds\\title.wav");
				this.musicaFondo.PlayLooping();
				return true;
			} else {
				return false;
			}
		}
		
		// Crea el Evento para el Boton de búsqueda
		void BusquedaBTNClick(object sender, EventArgs e) {
			// En caso de estar vació el texto o de no cumplir con la expresión regular, se le informa a usuario
			if(this.busquedaTxtBox.Text == "Ejemplo: A,1" || !(new Regex("^[A-Z]+,[0-9]+$")).IsMatch(this.busquedaTxtBox.Text)){
				ToolTip tt = new ToolTip();
    			tt.Show("Ingrese la coordenada por favor.", this.busquedaTxtBox, 2000);
				this.busquedaTxtBox.Focus();
				return;
			}
			// Se evalúa si la coordenada ingresada puede existir en el mapa
			string [] coordenadas = this.busquedaTxtBox.Text.Split(',');
			int columna = ((int)char.Parse(coordenadas[0]) - 65);
			int fila = (Int32.Parse(coordenadas[1]));
			// En caso de que la columna se salga de los límites del mapa manda error
			if(columna > this.c - 1){
				ToolTip t_Tip = new ToolTip();
				t_Tip.Active = true; 
				t_Tip.AutoPopDelay = 4000; 
				t_Tip.InitialDelay = 600; 
				t_Tip.IsBalloon = true; 
				t_Tip.ToolTipIcon = ToolTipIcon.Info; 
				t_Tip.Show("La columna fuera de los límites del mapa." +
				           "\nIntente una nueva coordenada.", this.busquedaTxtBox, 0, -90, 2000);
				this.busquedaTxtBox.Focus();
				return;
			}
			// En caso de que la fila se salga de los límites del mapa manda error
			if(fila > this.f || fila < 1){
				ToolTip t_Tip = new ToolTip();
				t_Tip.Active = true; 
				t_Tip.AutoPopDelay = 4000; 
				t_Tip.InitialDelay = 600; 
				t_Tip.IsBalloon = true; 
				t_Tip.ToolTipIcon = ToolTipIcon.Info; 
				t_Tip.Show("La fila fuera de los límites del mapa." +
				           "\nIntente una nueva coordenada.", this.busquedaTxtBox, 0, -90, 2000);
				this.busquedaTxtBox.Focus();
				return;
			}
			// Si todo salío correctamente, muestra la casilla con sus datos.
			MessageBox.Show("(" + this.busquedaTxtBox.Text + ") - " +
			                "Terreno: " + this.diccionarioIdsTexturas[this.mapaMatriz[fila - 1,columna]].ToString(), "¡Casilla encontrada!",
			                MessageBoxButtons.OK, MessageBoxIcon.Information);
		}
		
		// Evento para llamar el form del orden de movimiento
		void OrdenMovimientoBtnClick(object sender, EventArgs e) {
			FormOrdenMovimiento();
		}
		
		// Función para crear el form del orden de movimiento
		private void FormOrdenMovimiento(){
			this.ordenMovComboB.Clear();
			
			this.formAuxiliar = new Form();
			formAuxiliar.StartPosition = FormStartPosition.CenterScreen;
			formAuxiliar.Name = "formOrdenDeMovimiento";
			formAuxiliar.Text = "¡Elije el orden de movimiento!";
			formAuxiliar.FormBorderStyle = FormBorderStyle.FixedDialog;
			formAuxiliar.MaximizeBox = false;
			formAuxiliar.MinimizeBox = false;
			formAuxiliar.AutoSize = true;
			string path = Directory.GetCurrentDirectory();
			formAuxiliar.Icon = new Icon(path + "\\icons\\triforce.ico");
			
			// Crea Label
			Label l = new Label();
			l.SetBounds(5, 20, 0, 0);
			l.Text = "Orden de expansión";
			l.Text = "Orden de movimiento";
			l.Font = new Font("Arial", 19, FontStyle.Bold);
			l.Size = new Size (l.PreferredWidth, l.PreferredHeight);
			l.Parent = formAuxiliar;
			// ToolTip para los elementos de texturas
			ToolTip formToolTip = new ToolTip(); 
	        formToolTip.Active = true; 
	        formToolTip.AutoPopDelay = 4000; 
	        formToolTip.InitialDelay = 600; 
	        formToolTip.ToolTipIcon = ToolTipIcon.Info;
	        
			// Crea un Button para guardar la información
			Button btn = new Button();
        	btn.Name = "ordenMovimientoBtn";
        	btn.SetBounds(240,150,40,40);
        	path = Directory.GetCurrentDirectory() + "\\icons\\save.png";
        	btn.BackgroundImage = Image.FromFile(path);
        	btn.BackgroundImageLayout = ImageLayout.Stretch;
        	btn.Click += new EventHandler(GuardarOrdenMovimiento);
        	btn.Parent = formAuxiliar;
        	formToolTip.SetToolTip(btn, "Guardar configuración de " +
        	                       "\norden de movimiento.");
        	
        	for (int i = 0, distancia = 0; i < 4; i++, distancia += 70) {
        		// Se crean los Picture box
				PictureBox pb = new PictureBox();
				pb.SetBounds(10 + distancia, 55 , 60,60);
				pb.SizeMode = PictureBoxSizeMode.StretchImage;
				pb.Parent = formAuxiliar;
				
				// Combobox para elegir al personaje.
	        	ComboBox cb = new ComboBox();
	        	cb.SetBounds(10 + distancia, 125,60,60);
	        	cb.Items.Insert(0, "Primero");
	        	cb.Items.Insert(1, "Segundo");
	        	cb.Items.Insert(2, "Tercero");
	        	cb.Items.Insert(3, "Cuarto");
	        	cb.DropDownStyle = ComboBoxStyle.DropDownList;
	        	cb.SelectedIndex = this.ordenMovimiento[i];
	        	cb.SelectedIndexChanged += new EventHandler(AcomodarCBOrdenMovimiento);
	        	cb.Parent = formAuxiliar;
	        	switch(i){
					case 0:
					    pb.Name = "arribaPB";
					    pb.BorderStyle = BorderStyle.FixedSingle;
						pb.Image = Image.FromFile(Directory.GetCurrentDirectory() +"\\icons\\Arriba.png");
						cb.Name = "arribaCB" ;
						break;
					case 1:
						pb.Name = "derechaPB";
					    pb.BorderStyle = BorderStyle.FixedSingle;
						pb.Image = Image.FromFile(Directory.GetCurrentDirectory() +"\\icons\\Derecha.png");
						cb.Name = "derechaCB" ;
						break;
					case 2:
						pb.Name = "abajoPB";
					    pb.BorderStyle = BorderStyle.FixedSingle;
						pb.Image = Image.FromFile(Directory.GetCurrentDirectory() +"\\icons\\Abajo.png");
						cb.Name = "abajoCB" ;
						break;
					case 3:
						pb.Name = "izquierdaPB";
					    pb.BorderStyle = BorderStyle.FixedSingle;
						pb.Image = Image.FromFile(Directory.GetCurrentDirectory() +"\\icons\\Izquierda.png");
						cb.Name = "izquierdaCB" ;
						break;
						
				}
	        	this.ordenMovComboB.Add(i, cb);
        	}
        	this.formAuxiliar.ShowDialog();
		}
		
		// Evento para guardar el orden de movimiento
		private void GuardarOrdenMovimiento(Object o, EventArgs e){
			for (int i = 0; i < this.ordenMovimiento.Count; i++) {
				this.ordenMovimiento[i] = this.ordenMovComboB[i].SelectedIndex;
			}
			this.formAuxiliar.Dispose();
		}
		
		// Evento para los ComboBox y cambiar el Index seleccionado entre ellos
		private void AcomodarCBOrdenMovimiento(Object o, EventArgs e){
			ComboBox cb = (ComboBox)o;
			ComboBox cbAuxiliar = new ComboBox();
			List<int> lIndex = new List<int>(){0,1,2,3};
			foreach(KeyValuePair<int, ComboBox> entry in this.ordenMovComboB) {
				if (cb.SelectedIndex == entry.Value.SelectedIndex && cb != entry.Value){
					cbAuxiliar = entry.Value;
				}
				else{
					lIndex.Remove(entry.Value.SelectedIndex);
				}
			}
			if(lIndex.Count != 0){
				cbAuxiliar.SelectedIndex = lIndex[0];
			}
		}
		
		void TipoBusquedaCBSelectedIndexChanged(object sender, EventArgs e) {
			if(this.tipoBusquedaCB.SelectedIndex == 3 || this.tipoBusquedaCB.SelectedIndex == 4){
				FormMedidas();
				this.distanciaButton.Enabled = true;
			}	
			else{
				this.distanciaButton.Enabled = false;
			}
		}
		
		// Función para crear el form del orden de movimiento
		private void FormMedidas(){
			this.formAuxiliar = new Form();
			formAuxiliar.StartPosition = FormStartPosition.CenterScreen;
			formAuxiliar.Name = "formMedidas";
			formAuxiliar.Text = "¡Elije el tipo de medida!";
			formAuxiliar.FormBorderStyle = FormBorderStyle.FixedDialog;
			formAuxiliar.MaximizeBox = false;
			formAuxiliar.MinimizeBox = false;
			formAuxiliar.AutoSize = true;
			string path = Directory.GetCurrentDirectory();
			formAuxiliar.Icon = new Icon(path + "\\icons\\triforce.ico");
			
			// Crea Label
			Label l = new Label();
			l.SetBounds(5, 20, 0, 0);
			l.Text = "Medida disponibles";
			l.Font = new Font("Arial", 19, FontStyle.Bold);
			l.Size = new Size (l.PreferredWidth, l.PreferredHeight);
			l.Parent = formAuxiliar;
			
			// ToolTip para los elementos 
			ToolTip formToolTip = new ToolTip(); 
	        formToolTip.Active = true; 
	        formToolTip.AutoPopDelay = 4000; 
	        formToolTip.InitialDelay = 600; 
	        formToolTip.ToolTipIcon = ToolTipIcon.Info;
	     
			// Crea un Button para guardar la Medida Manhattan
			Button btn = new Button();
        	btn.Name = "medidaManhattanBtn";
        	btn.Text = "Manhattan";
        	btn.Font = new Font("Arial", 15, FontStyle.Bold);
        	btn.SetBounds(10,70,150,40);
        	btn.Click += new EventHandler(GuardarMedida);
        	btn.Parent = formAuxiliar;
        	formToolTip.SetToolTip(btn, "Medida Manhattan = |x2 - x1| + |y2 - y1|.");
        	
        	// Crea un Button para guardar la Medida Euclidiana
			Button btn_1 = new Button();
        	btn_1.Name = "medidaEuclidianaBtn";
        	btn_1.Text = "Euclidiana";
        	btn_1.Font = new Font("Arial", 15, FontStyle.Bold);
        	btn_1.SetBounds(160,70,150,40);
        	btn_1.Click += new EventHandler(GuardarMedida);
        	btn_1.Parent = formAuxiliar;
        	formToolTip.SetToolTip(btn_1, "Medida Euclidiana = ((x2 - x1)^2 + (y2 - y1)^2) ^ 1/2.");
        	
        	this.formAuxiliar.ShowDialog();
		}
		
		// Crea el evento que eliminará el Form y asignará el personaje con el que se jugará
		private void GuardarMedida(Object sender, EventArgs e){
			Button btn = (Button)sender;
			if(btn.Name == "medidaManhattanBtn"){
				this.medidaOpc = 0;
			}
			else{
				this.medidaOpc = 1;
			}
			this.formAuxiliar.Dispose();
		}
		
		void DistanciaButtonClick(object sender, EventArgs e) {
			FormMedidas();
		}
	}
}