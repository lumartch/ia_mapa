﻿/*
 * Created by SharpDevelop.
 * User: lumar
 * Date: 03/03/2020
 * Time: 08:54 p. m.
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Windows.Forms;

namespace ia_mapa
{
	/// <summary>
	/// Class with program entry point.
	/// </summary>
	internal sealed class Program
	{
		/// <summary>
		/// Program entry point.
		/// </summary>
		[STAThread]
		private static void Main(string[] args)
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			var icon = new NotifyIcon();
			Application.Run(new MainForm());
		}
		
	}
}
